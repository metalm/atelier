[/] Composants Swing
[/] Ajout de l'exemple idil.liv et du fichier idil : scène1.xml
[/] Ajout du verbe 'Peindre' (un format IDIL : Injection, Déclaration d'Interface Linotte)
[/] Ajout du verbe 'Ne pas faire réagir'
[/] Gestion des erreurs, ajout du verbe Essayer
[/] Ajout de l'espèce 'mégalithe'
[/] Ajout de l'espèce graphique chemin
[/] Ajout du verbe 'déboguer'
[/] Ajout du verbe 'retourner'
[/] Ajout du produits cartésiens de acteurs : affiche "Bonjour" & "Au revoir !"
[/] Ajout de l'espèce graphique polygone (ajout de l'exemple polygone.liv)
[ ] Ajout du pointeur invisible
[ ] Retour de l'applet...
[ ] Le verbe Appeler peut prendre des doublures ( évolution de $imon) cf course.liv
[ ] Le verbe Appeler peut appeler plusieurs fois le même paragraphe ( évolution de $imon)
[/] Ajout de l'arobase
[/] Caractère unicode
[/] Drapeau
[/] verbe Annihiler
