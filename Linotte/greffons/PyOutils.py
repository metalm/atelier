from org.linotte.greffons.externe import Greffon

class PyOutils(Greffon):

	# Retourne la valeur en unicode les caracteres ascii seulement
	def slot_ascii2unicode(self,v):
		retour = ord(v)
		retour = repr(unichr(retour))
		retour = retour[2:-1]
		if (len(retour)==1):
			return retour
		else:
			retour = retour.replace("x","u00",1)
			retour = retour.lower()
			return retour
                                                                                     
	# Retourne la valeur en unicode les caracteres ascii seulement
	def slot_unicode2ascii(self,v):
		retour = chr(eval("0x"+ v[4:6]))
		return retour