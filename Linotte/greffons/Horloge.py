from java.awt import Color, BasicStroke, Graphics2D
from org.linotte.greffons.externe import Graphique
import math
import time

class Horloge(Graphique):

	#### Attributs de l'objet Horloge
	heure = 0
	minute = 0
	seconde = 0

	#### Methodes liees a l aspect graphique
		
	def projette(self, dessin):
		x = self.getAttributeAsBigDecimal("x").intValue()
		y = self.getAttributeAsBigDecimal("y").intValue()
		rayon = self.getAttributeAsBigDecimal("rayon").intValue()		
		dessin.setPaint(Color.DARK_GRAY)
		dessin.setStroke(BasicStroke(3))      
		dessin.drawOval(x, y, rayon * 2, rayon * 2)
		x2 = x + rayon
		y2 = y + rayon
		# Affichage de l'heure
		dessin.setPaint(Color.BLUE)
		dessin.setStroke(BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND))
		dessin.drawLine(x2, y2, int(x2 + rayon * 0.6 * math.cos((self.heure - 3) * 2 * math.pi / 12)), int(y2 + rayon * 0.6 * math.sin((self.heure - 3) * 2 * math.pi / 12)))
		# Affichage des minutes
		dessin.setPaint(Color.BLACK)
		dessin.setStroke(BasicStroke(3, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND))
		dessin.drawLine(x2, y2, int(x2 + rayon * 0.85 * math.cos((self.minute - 15) * 2 * math.pi / 60)), int(y2 + rayon * 0.85 * math.sin((self.minute - 15) * 2 * math.pi / 60)))
		# Affichage de la trotteuse
	   	dessin.setPaint(Color.RED)
		dessin.setStroke(BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND))
	   	dessin.drawLine(x2, y2, int(x2 + rayon * 0.9 * math.cos((self.seconde - 15) * 2 * math.pi / 60)), int(y2 + rayon * 0.9 * math.sin((self.seconde - 15) * 2 * math.pi / 60)))

		# Les chiffres
	   	dessin.setPaint(Color.BLACK)
		for c in range(1, 13):
			dessin.drawString(str(c), int(x2 + rayon * 1.2 * math.cos((c - 15) * 2 * math.pi / 12)), int(y2 + rayon * 1.2 * math.sin((c - 15) * 2 * math.pi / 12)))
		
		return None		

	def getShape(self):
		return None
	
	#### Methodes liees au greffon Labyrinthe

	def slot_heure(self, h):
		self.heure = int(h)
		# Force le rafraichissement de la toile :
		self.getRessourceManager().setChangement()
		return True

	def slot_minute(self, m):
		self.minute = int(m)
		# Force le rafraichissement de la toile :
		self.getRessourceManager().setChangement()
		return True

	def slot_seconde(self, s):
		self.seconde = int(s)
		# Force le rafraichissement de la toile :
		self.getRessourceManager().setChangement()
		return True
