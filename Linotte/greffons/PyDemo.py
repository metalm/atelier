from org.linotte.greffons.externe import Graphique
from java.awt import Color

class PyDemo(Graphique):
		
	def projette(self, g):
	
		g.setColor(Color(125, 167, 116))
		g.fillRect(10, 15, 90, 60)

		g.setColor(Color(42, 179, 231))
		g.fillRect(130, 15, 90, 60)

		g.setColor(Color(70, 67, 123))
		g.fillRect(250, 15, 90, 60)

		g.setColor(Color(130, 100, 84))
		g.fillRect(10, 105, 90, 60)

		g.setColor(Color(252, 211, 61))
		g.fillRect(130, 105, 90, 60)

		g.setColor(Color(241, 98, 69))
		g.fillRect(250, 105, 90, 60)

		g.setColor(Color(217, 146, 54))
		g.fillRect(10, 195, 90, 60)

		g.setColor(Color(63, 121, 186))
		g.fillRect(130, 195, 90, 60)

		g.setColor(Color(31, 21, 1))
		g.fillRect(250, 195, 90, 60)
		
		g.drawString(self.getAttributeAsString("texte"), 10, 300);

	def getShape(self):
		return None


