# coding=utf8

from org.linotte.greffons.externe import Greffon

class Abaque(Greffon):
	

	# retourne un nombre aléatoire entre a et b exclu
	def slot_hasard(self,a,b):
		return None

	#
	# ARITHMÉTIQUE
	#

	# retourne VRAI si n est pair et FAUX sinon avec n entier positif ou nul
	def slot_pair(self,n):
		return n%2

	# retourne VRAI si n est impair et FAUX sinon avec n entier positif ou nul
	def slot_impair(self,n):
		return None

	# retourne le quotient de m par n avec m entier et n entier non nul
	def slot_quotient(self,m,n):
		return None

	# retourne le reste de la division de m par n avec m entier et n entier non nul
	def slot_reste(self,m,n):
		return None

	# retourne le plus grand commun diviseur de m et n avec m et n entiers
	def slot_pgcd(self,m,n):
		return None

	# retourne le plus petit commun multiple de m et n avec m et n entiers
	def slot_ppcm(self,m,n):
		return None

	# retourne VRAI si n est premier et FAUX sinon avec n entier positif ou nul
	def slot_premier(self,n):
		return None

	# retourne la suite des nombres premiers inférieurs à n avec n entier positif ou nul
	def slot_listepremiers(self,n):
		return None

	# retourne la suite des diviseurs du nombre n avec n entier positif
	def slot_diviseurs(self,n):
		return None

	# retourne la suite des facteurs premiers de n avec n entier positif ou nul
	def slot_factorise(self,n):
		return None

	# transforme le nombre m écrit en base b1 sous forme de chaîne de caractères en un nombre écrit en base b2 lui aussi sous forme de chaîne de caractères
	def slot_changebasee(self,m,b1,b2):
		return None

	#
	# PROBABILITÉS ET STATISTIQUES
	#

	# retourne la factorielle de n avec n entier positif ou nul
	def slot_factorielle(self,n):
		return None

	# retourne le nombre d’arrangements de p éléments pris parmi n avec n et p entier positifs ou nuls et p ≤ n
	def slot_arrangement(self,n,p):
		return None

	# retourne le nombre de combinaisons de p éléments pris parmi n avec n et p entier positifs ou nuls et p ≤ n
	def slot_combinaison(self,n,p):
		return None

	# retourne un vecteur de p tirages entiers pris entre m et n avec p entier positif, m et n entiers et m ≤ n
	def slot_tirageentier(self,p,m,n):
		return None

	# retourne un vecteur de p tirages réels pris entre a et b avec p entier positif, a et b réels et a ≤ b
	def slot_tiragereel(self,p,m,n):
		return None

	# retourne la fréquence de n dans la suite de nombres s avec n entier
	def slot_frequence(self,n,s):
		return None

	# retourne la suite des fréquences de p tirages entiers pris entre m et n avec p entier positif, m et n entiers et m ≤ n.
	def slot_frequencetirageentier(self,p,m,n):
		return None

	# retourne la moyenne du vecteur de nombres v
	def slot_moyenne(self,v):
		return None

	# retourne la moyenne du vecteur de nombres v pondéré par le vecteur de nombres n avec v et n de même dimension et n un vecteur de nombres positifs ou nuls mais non tous nuls
	def slot_moyenneponderee(self,v,n):
		return None

	# retourne l’écart type du vecteur de nombres v
	def slot_ecarttype(self,v):
		return None

	# retourne l’écart type du vecteur de nombres v pondéré par le vecteur de nombres n avec v et n de même dimension et n un vecteur de nombres positifs ou nuls mais non tous nuls
	def slot_ecarttypeponderee(self,v,n):
		return None

	# retourne la variance du vecteur de nombres v
	def slot_variance(self,v):
		return None

	# retourne la variance du vecteur de nombres v pondéré par le vecteur de nombres n avec v et n de même dimension et n un vecteur de nombres positifs ou nuls mais non tous nuls
	def slot_varianceponderee(self,v,n):
		return None

	# retourne la médiane du vecteur de nombres v
	def slot_mediane(self,v):
		return None

	# retourne la médiane du vecteur de nombres v pondéré par le vecteur de nombres n avec v et n de même dimension et n un vecteur de nombres positifs ou nuls mais non tous nuls
	def slot_medianeponderee(self,v,n):
		return None

	# retourne la médiane du vecteur de nombres v pondéré par le vecteur de nombres n avec v et n de même dimension et n un vecteur de nombres positifs ou nuls mais non tous nuls
	def slot_medianeponderee(self,v,n):
		return None

	# retourne les deux quartiles du vecteur de nombres v
	def slot_quartiles(self,v):
		return None

	# retourne les deux quartiles du vecteur de nombres v pondéré par le vecteur de nombres n avec v et n de même dimension et n un vecteur de nombres positifs ou nuls mais non tous nuls
	def slot_quartilesponderee(self,v,n):
		return None

	# retourne les déciles D1 et D9 du vecteur de nombres v
	def slot_deciles(self,v):
		return None

	# retourne les déciles D1 et D9 du vecteur de nombres v pondéré par le vecteur de nombres n avec v et n de même dimension et n un vecteur de nombres positifs ou nuls mais non tous nuls
	def slot_decilesponderee(self,v,n):
		return None

	# retourne la probabilité p(X ≤ t) lorsque X suit la loi exponentielle de paramètre λ avec λ positif
	def slot_loiexp(self,l,t):
		return None

	# retourne la probabilité p(X ≤ t) lorsque X suit la loi normale de paramètres et σ avec σ positif
	def slot_loinormale(self,l,xbar,t):
		return None

	# retourne les coefficients a et b de la droite de régression de y en x par la méthode des moindres carrés, d’équation y = ax + b, avec x et y des vecteurs de nombres de même dimension et x un vecteur de nombres non tous égaux
	def slot_regressionyenx(self,x,y):
		return None

	#
	# Outils
	#

	# retourne le vecteur v avec une seule occurrence de ses éléments dupliqués
	def slot_uniques(self,v):
		return None

	# retourne la somme de tous les éléments du vecteur v
	def slot_somme(self,v):
		return None

	# retourne le produit de tous les éléments du vecteur v
	def slot_produit(self,v):
		return None
