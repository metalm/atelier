from org.linotte.greffons.externe import Graphique
from java.awt import Color
from java.awt import GradientPaint

import time

class Labyrinthe(Graphique):

	#### Attributs de l'objet Labyrinthe
	
	# Etat du personnage
	position_x = -1
	position_y = -1
	
	# Etat du jeu
	modele = []
	clef = False

	# Affichage
	message = "J'attends vos ordres !"

	#### Methodes liees a l aspect graphique
		
	def projette(self, g):
			
		# Quadrillage :			
		longueur = self.getAttributeAsBigDecimal("longueur").intValue()
		largeur = self.getAttributeAsBigDecimal("largeur").intValue()
		
		for i in range(0,longueur):
			for j in range(0,largeur):		
				piece = self.modele[i+longueur*j]
				## Premiere couche
				if piece == 0 : 
					# Sol
					g.setColor(Color.GRAY)
				if piece == 1 :
					# Mur 
					g.setColor(Color.RED)
				if piece == 3 : 
					# Clef
					g.setColor(Color.DARK_GRAY)
				if piece == 4 : 
					# Porte
					g.setColor(Color.BLACK)
				if piece == 6 :
					# Sol avec un pas 
					g.setColor(Color.GRAY)
				g.fill3DRect (i*55, j*55, 50, 50, True)

				## Deuxieme couche
				if piece == 6 : 
					# Affichage des pas :
					g.setColor(Color.BLACK)
					g.fillRect(i*55+20, j*55+20, 10, 10)
				if piece == 3 : 
					# Clef
					g.setColor(Color.BLUE)
					g.fillOval(i*55+10, j*55+30, 15, 15)
					g.fillRect(i*55+15, j*55+30, 30, 5)
					g.fillRect(i*55+40, j*55+30, 5, 15)
					g.fillRect(i*55+33, j*55+30, 5, 15)
				if piece == 4 : 
					# Porte
					g.setColor(Color.WHITE)
					g.fillRect(i*55+5, j*55+20, 10, 3)
			
		# Robot :
		g.setColor(Color.YELLOW)
		g.fillOval(self.position_x*55, self.position_y*55, 47, 47)
		g.setColor(Color.ORANGE)
		g.fillRect(self.position_x*55, self.position_y*55+40, 10, 10)
		g.fillRect(self.position_x*55+37, self.position_y*55+40, 10, 10)
		g.setColor(Color.BLACK)
		g.fillOval(self.position_x*55+10, self.position_y*55+10, 5, 5)
		g.fillOval(self.position_x*55+35, self.position_y*55+10, 5, 5)
	
		# Message :
		g.setColor(Color.BLACK)
		g.drawString(self.message, 10, largeur * 55 + 20);

		# Message clef :
		if self.clef == True :
			g.setColor(Color.BLACK)
			g.drawString("J'ai une clef.", 10, largeur * 55 + 40);

	def getShape(self):
		return None
	
	#### Methodes liees au greffon Labyrinthe

	def getShape(self):
		return None

	def slot_affiche(self,tableau):
		self.modele = tableau
		# Force le rafraichissement de la toile :
		self.getRessourceManager().setChangement()
		return False

	def slot_poserobot(self,x,y):
		self.position_x = int(x) - 1
		self.position_y = int(y) - 1
		self.afficheOrdre("On peut commencer chef !")
		return False

	def slot_affiche(self,tableau):
		self.modele = tableau
		# Force le rafraichissement de la toile :
		self.getRessourceManager().setChangement()
		return False

	def slot_droite(self):
		self.prepareOrdre("action : droite")
		self.position_x = self.position_x + 1
		longueur = self.getAttributeAsBigDecimal("longueur").intValue()
		piece = self.modele[self.position_x+longueur*self.position_y]
		if piece != 1 :
			if self.modele[self.position_x-1+longueur*self.position_y] == 0 :
				self.modele[self.position_x-1+longueur*self.position_y] = 6
			self.afficheOrdre("Ok, je vais \xe0 droite !")
		else :
			self.position_x = self.position_x - 1
			self.afficheOrdre("Pas possible !")		
		return False
		
	def slot_gauche(self):
		self.prepareOrdre("action : gauche")
		self.position_x = self.position_x - 1
		longueur = self.getAttributeAsBigDecimal("longueur").intValue()
		piece = self.modele[self.position_x+longueur*self.position_y]
		if piece != 1 :
			if self.modele[self.position_x+1+longueur*self.position_y] == 0 :
				self.modele[self.position_x+1+longueur*self.position_y] = 6
			self.afficheOrdre("Ok, je vais \xe0 gauche !")
		else :
			self.position_x = self.position_x + 1
			self.afficheOrdre("Pas possible !")		
		
		return False

	def slot_haut(self):
		self.prepareOrdre("action : haut")
		self.position_y = self.position_y - 1
		longueur = self.getAttributeAsBigDecimal("longueur").intValue()
		piece = self.modele[self.position_x+longueur*self.position_y]
		if piece != 1 :
			if self.modele[self.position_x+longueur*(self.position_y+1)] == 0 :
				self.modele[self.position_x+longueur*(self.position_y+1)] = 6
			self.afficheOrdre("Ok, je vais en haut !")
		else :
			self.position_y = self.position_y + 1
			self.afficheOrdre("Pas possible !")		
		
		return False
		
	def slot_bas(self):
		self.prepareOrdre("action : bas")
		self.position_y = self.position_y + 1
		longueur = self.getAttributeAsBigDecimal("longueur").intValue()
		piece = self.modele[self.position_x+longueur*self.position_y]
		if piece != 1 :
			if self.modele[self.position_x+longueur*(self.position_y-1)] == 0 :
				self.modele[self.position_x+longueur*(self.position_y-1)] = 6
			self.afficheOrdre("Ok, je vais en bas !")
		else :
			self.position_y = self.position_y - 1
			self.afficheOrdre("Pas possible !")		
		
		return False
				
	def slot_ouvre(self):
		self.prepareOrdre("action : ouvre")
		longueur = self.getAttributeAsBigDecimal("longueur").intValue()
		piece = self.modele[self.position_x+longueur*self.position_y]
		if piece == 4 and self.clef == True :
			self.clef = False
			self.modele[self.position_x+longueur*self.position_y] = 0
			self.afficheOrdre("Ok, j'ai ouvert la porte !")
		else :
			self.afficheOrdre("Pas possible !")
		return False

	def slot_prend(self):
		self.prepareOrdre("action : prend")
		longueur = self.getAttributeAsBigDecimal("longueur").intValue()
		piece = self.modele[self.position_x+longueur*self.position_y]
		if piece == 3 :
			self.modele[self.position_x+longueur*self.position_y] = 0
			self.clef = True
			self.afficheOrdre("Ok, je prends la clef !")
		else :
			self.afficheOrdre("Pas possible !")
		return False
		
	def slot_quitte(self):
		self.prepareOrdre("action : quitte")
		self.message = "Ok, au revoir chef !"
		self.getRessourceManager().setChangement()
		time.sleep(1)
		self.message = "Robot arrete"
		self.getRessourceManager().setChangement()
		return False
		
	def afficheOrdre (self,texte):
		self.message = texte
		# Force le rafraichissement de la toile :
		self.getRessourceManager().setChangement()
		time.sleep(1)
		self.getRessourceManager().setChangement()
		self.message = "J'attends vos ordres !"
		time.sleep(0.5)
		return
	
	def prepareOrdre (self,texte):
		self.message = texte
		# Force le rafraichissement de la toile :
		self.getRessourceManager().setChangement()
		time.sleep(0.5)
		return