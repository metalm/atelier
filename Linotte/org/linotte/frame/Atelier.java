/***********************************************************************
 * Linotte                                                             *
 * Version release date : July 30, 2008                                *
 * Author : Mounes Ronan ronan.mounes@amstrad.eu                       *
 *                                                                     *
 *     http://langagelinotte.free.fr                                   *
 *                                                                     *
 * This code is released under the GNU GPL license, version 2 or       *
 * later, for educational and non-commercial purposes only.            *
 * If any part of the code is to be included in a commercial           *
 * software, please contact us first for a clearance at                *
 *   ronan.mounes@amstrad.eu                                           *
 *                                                                     *
 *   This notice must remain intact in all copies of this code.        *
 *   This code is distributed WITHOUT ANY WARRANTY OF ANY KIND.        *
 *   The GNU GPL license can be found at :                             *
 *           http://www.gnu.org/copyleft/gpl.html                      *
 *                                                                     *
 ***********************************************************************/

package org.linotte.frame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.Border;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.rtf.RTFEditorKit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import org.alize.kernel.AKPatrol;
import org.alize.kernel.AKRuntime;
import org.jdesktop.swingx.JXTextField;
import org.linotte.frame.atelier.Apropos;
import org.linotte.frame.atelier.BoiteRecherche;
import org.linotte.frame.atelier.FiltreLivre;
import org.linotte.frame.atelier.FlatIHM;
import org.linotte.frame.atelier.FrameDialog;
import org.linotte.frame.atelier.FrameIHM;
import org.linotte.frame.atelier.Inspecteur;
import org.linotte.frame.atelier.MemoryMonitor;
import org.linotte.frame.atelier.Merci;
import org.linotte.frame.atelier.SliderMenuItem;
import org.linotte.frame.atelier.Updater;
import org.linotte.frame.atelier.VerticalGridLayout;
import org.linotte.frame.cahier.Cahier;
import org.linotte.frame.cahier.Cahier.EtatCachier;
import org.linotte.frame.cahier.Onglets;
import org.linotte.frame.cahier.sommaire.JPanelSommaire;
import org.linotte.frame.coloration.StyleBuilder;
import org.linotte.frame.coloration.StyleLinotte;
import org.linotte.frame.coloration.StyleManageur;
import org.linotte.frame.favoris.GestionnaireFavoris;
import org.linotte.frame.greffons.GreffonsManager;
import org.linotte.frame.gui.JPanelBackGround;
import org.linotte.frame.gui.JTextPaneText;
import org.linotte.frame.gui.PopupListener;
import org.linotte.frame.gui.SplashWindow;
import org.linotte.frame.latoile.Couleur;
import org.linotte.frame.latoile.JPanelLaToile;
import org.linotte.frame.latoile.Java6;
import org.linotte.frame.latoile.LaToile;
import org.linotte.frame.latoile.Toile;
import org.linotte.frame.moteur.ConsoleProcess;
import org.linotte.frame.moteur.Debogueur;
import org.linotte.frame.moteur.Formater;
import org.linotte.frame.moteur.FrameProcess;
import org.linotte.frame.outils.ArrowIcon;
import org.linotte.frame.outils.Tools;
import org.linotte.frame.projet.ExplorateurProjet;
import org.linotte.greffons.GestionDesGreffons;
import org.linotte.greffons.GreffonsChargeur;
import org.linotte.greffons.api.AKMethod;
import org.linotte.greffons.java.JavaMethod;
import org.linotte.implementations.LibrairieVirtuelleSyntaxeV2;
import org.linotte.moteur.entites.Prototype;
import org.linotte.moteur.exception.StopException;
import org.linotte.moteur.outils.FichierOutils;
import org.linotte.moteur.outils.JTextPaneToPdf;
import org.linotte.moteur.outils.LangageSwitch;
import org.linotte.moteur.outils.Preference;
import org.linotte.moteur.outils.Ressources;
import org.linotte.moteur.xml.Linotte;
import org.linotte.moteur.xml.RegistreDesActions;
import org.linotte.moteur.xml.Version;
import org.linotte.moteur.xml.alize.kernel.ContextHelper;
import org.linotte.moteur.xml.alize.kernel.RuntimeContext;
import org.linotte.moteur.xml.alize.kernel.Trace;
import org.linotte.moteur.xml.alize.kernel.audit.ChronoAudit;
import org.linotte.moteur.xml.alize.kernel.audit.JobAudit;
import org.linotte.moteur.xml.alize.kernel.audit.LibrairieVirtuelleAudit;
import org.linotte.moteur.xml.alize.kernel.audit.LogAudit;
import org.linotte.moteur.xml.alize.kernel.audit.ProcessusAudit;
import org.linotte.moteur.xml.alize.kernel.audit.RuntimeAudit;
import org.linotte.moteur.xml.alize.kernel.audit.ToileAudit;
import org.linotte.moteur.xml.alize.parseur.ParserContext;
import org.linotte.moteur.xml.alize.parseur.ParserContext.MODE;
import org.linotte.moteur.xml.alize.parseur.Parseur;
import org.linotte.moteur.xml.alize.parseur.XMLIterator;
import org.linotte.moteur.xml.alize.parseur.a.NExpression;
import org.linotte.moteur.xml.alize.parseur.a.Noeud;
import org.linotte.moteur.xml.analyse.SynonymeLoader;
import org.linotte.moteur.xml.analyse.multilangage.Langage;
import org.linotte.moteur.xml.api.Librairie;
import org.linotte.web.Run;
import org.w3c.dom.Element;

import console.Jinotte;

/**
 * Affichage de l'interface principale de L'atelier Linotte. Contient également
 * le "main" pour exécuter l'Atelier
 * 
 */
@SuppressWarnings("serial")
public class Atelier extends JFrame implements WindowListener {

	private static final int TAILLE_H = 800, TAILLE_V = 650;

	private static final String splash = "splashlinotte.png";

	private static SplashWindow splashWindow1 = null;

	private static Atelier atelier = null;

	private JPanel jPanelAtelier = null;

	private JSplitPane jSplitPaneAtelier = null;

	private JSplitPane splitPaneSommaire = null;

	public JSplitPane splitPaneProjet = null;

	public JTabbedPane tabbedPaneNavigateur = null;

	// //////////////////////////////////////////////////////////////// Boutons

	private JButton jButtonLire = null;

	private JButton jButtonTester = null;

	private JButton jButtonPasAPas = null;

	private JButton jButtonContinuer = null;

	private JButton jButtonUpdate = null;

	private JButton jButtonStop = null;

	private JButton jButtonTimbre = null;

	public JButton jButtonRanger = null;

	private JButton jButtonLibrairie = null;

	private JPopupMenu popupTableau;

	private JMenuItem itemPopupTableau;

	private JMenuItem itemPopupTableau2;

	private Document sortieTableau = null;

	private Document sortieAudit = null;

	private Apropos apropos = null;

	private Merci merci = null;

	private FrameIHM dialogframeihm = null;

	private Cahier cahierCourant = null;

	private JScrollPane jScrollPaneTableau = null;

	private JScrollPane jScrollPaneAudit = null;

	private JTextPaneText jEditorPaneTableau = null;

	private JTextPaneText jEditorPaneAudit = null;

	private JFileChooser fileChooser_ouvrir = null;

	private JFileChooser fileChooser_sauvegarder = null;

	private JFileChooser fileChooser_exporter = null;

	private JFileChooser fileChooser_exporterPNG = null;

	private JFileChooser fileChooser_exporterHTML = null;

	private JFileChooser fileChooser_exporterRTF = null;

	private JFileChooser fileChooser_greffonJava = null;

	public UndoAction undoAction;

	public RedoAction redoAction;

	// ////////////////////////////////////////////////////////////////// Menus

	private JMenuBar jMenuBar = null;

	private JMenu jMenuEdition = null;

	private JMenu jMenuOutils = null;

	private JMenu jMenuBibliotheque = null;

	private JMenu jMenuLangage = null;

	private ButtonGroup groupCheckBoxLangage = null;

	private JMenu jMenuVerbier = null;

	private JMenu jMenuCondition = null;

	private JMenu jMenuMathematiques = null;

	private JMenu jMenuBoucle = null;

	private JMenu jMenuEspeces = null;

	private JMenu jMenuHistorisque = null;

	private JMenuItem jMenuItemDisque = null;

	private JMenuItem jMenuItemNouveau = null;

	private JMenuItem jMenuItemNouveauLivreGraphique = null;

	private JMenuItem jMenuItemRangerSous = null;

	private JMenuItem jMenuItemExporter = null;

	private JMenuItem jMenuItemExporterHTML = null;

	private JMenuItem jMenuItemExporterRTF = null;

	private JMenuItem jMenuItemExporterPNG = null;

	private JMenuItem jMenuItemPresent = null;

	private JMenuItem jMenuItemImperatif = null;

	private JMenuItem jMenuItemBonifieur = null;

	private SliderMenuItem jMenuItemDebogueur = null;

	private JMenuItem jMenuItemSaveWorkSpace = null;

	private JMenuItem jMenuItemRechercher = null;

	private JMenuItem jMenuItemFormater = null;

	private JMenuItem jMenuItemLaToile = null;

	private JMenuItem jMenuItemManageur = null;

	private JMenuItem jMenuItemGreffonLinotte = null;

	private JMenuItem jMenuItemManageurStyle = null;

	private JMenu jMenuLinotte = null;

	private JMenu jMenuThemes = null;

	private JMenuItem jMenuItemApropos = null;

	private JMenuItem jMenuItemMerci = null;

	private boolean saut2ligne = false;

	private CutL couper = new CutL();

	private CopyL copier = new CopyL();

	private PasteL coller = new PasteL();

	// Gestion multi-langage
	public static Linotte linotte = null;
	private LangageSwitch langageSwitch = null;

	// //////////////////////////////////////////////////////////// Maps pour le menu verbes

	private Map<String, String> verbes_present = new HashMap<String, String>();

	private Map<String, Set<String>> slots_prototype = new HashMap<String, Set<String>>();

	private Map<String, String> verbes_imperatif = new HashMap<String, String>();

	private Map<String, Boolean> verbes_icone = new HashMap<String, Boolean>();

	private Map<String, String> verbes_conditions = new HashMap<String, String>();

	private Map<String, String> verbes_mathematiques = new HashMap<String, String>();

	private Map<String, String> verbes_boucle = new HashMap<String, String>();

	private Map<String, String> verbes_especes = new HashMap<String, String>();

	private Map<String, String> verbes_aide = new HashMap<String, String>();

	private boolean affichagePresent = true;

	public static Font font;

	private Inspecteur inspecteur;

	private Clipboard clipbd = getToolkit().getSystemClipboard();

	private static LaToile toile;

	private static Librairie<?> librairie;

	private BoiteRecherche findAndReplace;

	// Gestion des onglets :

	private Onglets cahierOnglet;

	public static ExplorateurProjet explorateur;
	private Thread threadLancementAtelier4;

	// Théme :

	private Color menuBarColor = null;// Color.BLACK;
	private Color textBarColor = null;// Color.WHITE;
	private boolean menuBarBordure = false;// false;
	private int buttonTextHorizontale = SwingConstants.BOTTOM; // AbstractButton.TOP;
	private int buttonTextVerticale = SwingConstants.CENTER;// AbstractButton.RIGHT;

	private List<OuvrirLivreAction> listHistorique = new ArrayList<Atelier.OuvrirLivreAction>();

	private final class UpdateSuccessAction implements Updater.SuccessAction {
		public void testOK(String message) {
			jButtonUpdate.setVisible(true);
			jButtonUpdate.setToolTipText(message);
		}
	}

	public class UndoAction extends AbstractAction {
		public UndoAction() {
			super("Undo");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			try {
				getCahierCourant().setEditeurUpdate(true);
				getCahierCourant().getUndoManager().undo();
			} catch (CannotUndoException ex) {
				ex.printStackTrace();
			}
			updateUndoState();
			redoAction.updateRedoState();
		}

		public void updateUndoState() {
			if (getCahierCourant() != null && getCahierCourant().getUndoManager() != null && getCahierCourant().getUndoManager().canUndo()) {
				setEnabled(true);
				putValue(Action.NAME, "Annuler");
			} else {
				setEnabled(false);
				putValue(Action.NAME, "Annuler");
			}
		}
	}

	public class RedoAction extends AbstractAction {
		public RedoAction() {
			super("Redo");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			try {
				getCahierCourant().setEditeurUpdate(true);
				getCahierCourant().getUndoManager().redo();
			} catch (CannotRedoException ex) {
				ex.printStackTrace();
			}
			updateRedoState();
			undoAction.updateUndoState();
		}

		public void updateRedoState() {
			if (getCahierCourant() != null && getCahierCourant().getUndoManager() != null && getCahierCourant().getUndoManager().canRedo()) {
				setEnabled(true);
				putValue(Action.NAME, "Rétablir");
			} else {
				setEnabled(false);
				putValue(Action.NAME, "Rétablir");
			}
		}
	}

	public class CopyL extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			String selection = getCahierCourant().getSelectedText();
			if (selection == null)
				return;
			StringSelection clipString = new StringSelection(selection);
			clipbd.setContents(clipString, clipString);
		}
	}

	public class CutL extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			String selection = getCahierCourant().getSelectedText();
			if (selection == null)
				return;
			StringSelection clipString = new StringSelection(selection);
			clipbd.setContents(clipString, clipString);
			getCahierCourant().replaceSelection("");
		}
	}

	public class PasteL extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			Transferable clipData = clipbd.getContents(Atelier.this);
			try {
				String clipString = (String) clipData.getTransferData(DataFlavor.stringFlavor);
				getCahierCourant().replaceSelection(clipString);
			} catch (Exception ex) {
			}
		}
	}

	public static void main(String[] args) {

		try {
			//String HOTSPOT_BEAN_NAME = "com.sun.management:type=HotSpotDiagnostic";
			//MBeanServer server = ManagementFactory.getPlatformMBeanServer();
			//HotSpotDiagnosticMXBean bean;
			//bean = ManagementFactory.newPlatformMXBeanProxy(server, HOTSPOT_BEAN_NAME, HotSpotDiagnosticMXBean.class);
			//bean.setVMOption("add-exports", "java.desktop/sun.swing=ALL-UNNAMED");
			//bean.setVMOption("addExports", "java.desktop/sun.swing=ALL-UNNAMED");
			//List<VMOption> l = bean.getDiagnosticOptions();
			//for (VMOption vmOption : l) {
			//	System.out.println(vmOption.toString());
			//}
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		// --add-exports java.desktop/sun.swing=ALL-UNNAMED

		if (args != null && args.length > 0) {
			Preference.CAN_WRITE = true;
			Jinotte.main(args);
		} else {
			Preference.CAN_WRITE = true;
			try {

				if (Preference.getIntance().getProperty(Preference.P_STYLE) != null) {
					UIManager.setLookAndFeel(Preference.getIntance().getProperty(Preference.P_STYLE));
				} else {
					try {
						UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					} catch (Exception e) {
						UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
					}
				}

				Preference.getIntance().setProperty(Preference.P_STYLE, UIManager.getLookAndFeel().getClass().getName());

			} catch (Exception e) {
				e.printStackTrace();
				Preference.getIntance().remove(Preference.P_STYLE);
			}
			// JFrame.setDefaultLookAndFeelDecorated(true);

			Langage l = Gandalf.choisirLangage();
			if (l != null)
				Preference.getIntance().setProperty(Preference.P_LANGAGE, l.name());

			splashWindow1 = new SplashWindow(splash, new Frame(), 10);
			splashWindow1.setProgressValue(0, "Construction de l'environnement");
			createAndShowGUI();
		}
	}

	/**
	 * Cette méthode initialise l'affichage de la fenetre
	 */
	private static void createAndShowGUI() {

		Preference preference = Preference.getIntance();
		// Avant chargement de la fenetre :
		boolean auto = false;
		boolean latoile = false;
		boolean maximum = true;
		int px = 0, py = 0, pl = 0, ph = 0, tx = 0, ty = 0;// bh = 0, bv = 0;
		if (preference.getInt(Preference.P_HAUTEUR) != 0) {
			px = preference.getInt(Preference.P_X);
			py = preference.getInt(Preference.P_Y);
			ph = preference.getInt(Preference.P_HAUTEUR);
			pl = preference.getInt(Preference.P_LARGEUR);
			tx = preference.getInt(Preference.P_TOILE_X);
			ty = preference.getInt(Preference.P_TOILE_Y);
			latoile = preference.getBoolean(Preference.P_TOILE);
			maximum = preference.getBoolean(Preference.P_WINDOW_MAX);
			auto = true;

		} else {
			// comportement par defaut souhaite :
			preference.setBoolean(Preference.P_WINDOW_MAX, true);
		}

		splashWindow1.setProgressValue(1, "Chargement des thèmes");
		splashWindow1.setProgressValue(2, "Contruction de l'Atelier");

		Atelier atelier = new Atelier(getTitre()); // + " " + Version.getVersion()
		List<Image> l = new ArrayList<Image>();
		l.add(Ressources.getImageIcon("linotte_new.png").getImage());
		atelier.setIconImages(l);

		atelier.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		atelier.initialisationComposantsAtelier();

		// Display the window.
		atelier.setResizable(true);

		int taille = TAILLE_H + 10;
		int taille2 = taille + LaToile.LARGEUR + 10;
		int width = Toolkit.getDefaultToolkit().getScreenSize().width;
		int height = (Toolkit.getDefaultToolkit().getScreenSize().height - taille) / 2;

		if (maximum) {
			atelier.setExtendedState(JFrame.MAXIMIZED_BOTH);
		} else {

			if (auto) {
				atelier.setLocation(px, py);
				atelier.setPreferredSize(new java.awt.Dimension(pl, ph));
			} else {
				if (width > 1000) {
					atelier.setLocation((width - (taille2)) / 2, height);
				} else {
					atelier.setLocationRelativeTo(null);
				}
			}
		}

		if (auto) {
			toile = Toile.initToile(Atelier.linotte, true, atelier, false, false, latoile, tx, ty);
		} else {
			toile = Toile.initToile(Atelier.linotte, true, atelier, false, false, latoile, ((width - (taille2)) / 2) + taille, height);
		}

		// Pour l'audit :
		librairie.setToile(toile);

		// Ajout des audits :
		AKPatrol.auditeurs.add(new ToileAudit(toile));
		AKPatrol.auditeurs.add(new ChronoAudit());
		AKPatrol.auditeurs.add(new RuntimeAudit());
		AKPatrol.auditeurs.add(new JobAudit());
		AKPatrol.auditeurs.add(new ProcessusAudit());
		AKPatrol.auditeurs.add(new LibrairieVirtuelleAudit(librairie));
		AKPatrol.auditLog = new LogAudit(atelier);

		String info = SynonymeLoader.chargerSynonymes();

		atelier.pack();
		splashWindow1.setProgressValue(9, "Affichage de l'Atelier");
		atelier.setVisible(true);

		toile.setVisible(latoile);

		if (info != null) {
			atelier.ecrirelnTableau("Chargement du fichier de synonymes : " + info);
		}

		// Modification du comportement des tooltips :
		// ToolTipManager.sharedInstance().setReshowDelay(50000);
		// ToolTipManager.sharedInstance().setInitialDelay(2000);

		if (Atelier.explorateur.threadLancementAtelier1 != null)
			synchronized (Atelier.explorateur.threadLancementAtelier1) {
				Atelier.explorateur.threadLancementAtelier1.notify();
			}
		if (Atelier.explorateur.threadLancementAtelier2 != null)
			synchronized (Atelier.explorateur.threadLancementAtelier2) {
				Atelier.explorateur.threadLancementAtelier2.notify();
			}
		if (Atelier.explorateur.threadLancementAtelier3 != null)
			synchronized (Atelier.explorateur.threadLancementAtelier3) {
				Atelier.explorateur.threadLancementAtelier3.notify();
			}
		if (atelier.threadLancementAtelier4 != null)
			synchronized (atelier.threadLancementAtelier4) {
				atelier.threadLancementAtelier4.notify();
			}

		splashWindow1.setProgressValue(10, "Environnement prêt !");

		// Vérifier la modification des greffons en Linotte :

		GestionDesGreffons.verifierGreffons(atelier);

	}

	/**
	 * Crée un nouvel atelier Linotte
	 * 
	 * @param nom
	 *            Le titre de la fenêtre, usuellement "Atelier Linotte"
	 */
	public Atelier(String nom) {
		super(nom);
		atelier = this;
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	public void initialisationComposantsAtelier() {
		try {

			librairie = new LibrairieVirtuelleSyntaxeV2();
			splashWindow1.setProgressValue(3, "Chargement du fichier de grammaire");

			splashWindow1.setProgressValue(4, "Chargement des styles");
			new StyleLinotte();
			dialogframeihm = new FlatIHM(this);
			splashWindow1.setProgressValue(5, "Initialisation du moteur Linotte " + Version.getVERSION_TECHNIQUE());

			Langage langage = chargementLangageProgrammation();

			demarrageServeurHTTP();

			splashWindow1.setProgressValue(6, "Chargement des préférences");
			// Gestion de la police :
			String sfont = Preference.getIntance().getProperty(Preference.P_FONT);
			if (sfont == null) {
				sfont = "Consolas";
				Preference.getIntance().setProperty(Preference.P_FONT, sfont);
			}
			int sftaille = Preference.getIntance().getInt(Preference.P_TAILLE_FONT);
			if (sftaille == 0) {
				sftaille = 14;
				Preference.getIntance().setInt(Preference.P_TAILLE_FONT, sftaille);
			}
			font = new Font(sfont, Font.PLAIN, sftaille);

			undoAction = new UndoAction();
			redoAction = new RedoAction();

			boolean verbe = true, saveWorkSpace = true, bonifieur = false;
			int delais_pas_a_pas = 400;

			// Restauration des préférences :
			if (Preference.getIntance().get(Preference.P_MODE_VERBE) != null) {
				verbe = Preference.getIntance().getBoolean(Preference.P_MODE_VERBE);
			}

			if (Preference.getIntance().get(Preference.P_MODE_SAVE_WORKSPACE) != null) {
				saveWorkSpace = Preference.getIntance().getBoolean(Preference.P_MODE_SAVE_WORKSPACE);
			} else {
				Preference.getIntance().setBoolean(Preference.P_MODE_SAVE_WORKSPACE, saveWorkSpace);
			}

			if (Preference.getIntance().get(Preference.P_MODE_BONIFIEUR) != null) {
				bonifieur = Preference.getIntance().getBoolean(Preference.P_MODE_BONIFIEUR);
			} else {
				Preference.getIntance().setBoolean(Preference.P_MODE_BONIFIEUR, bonifieur);
			}

			if (Preference.getIntance().get(Preference.P_PAS_A_PAS) != null) {
				delais_pas_a_pas = Preference.getIntance().getInt(Preference.P_PAS_A_PAS);
			} else {
				Preference.getIntance().setInt(Preference.P_PAS_A_PAS, delais_pas_a_pas);
			}

			splashWindow1.setProgressValue(7, "Construction de l'Atelier");

			this.setContentPane(getJPanelAtelier());
			this.setJMenuBar(getxJMenuBar());
			this.setPreferredSize(new java.awt.Dimension(TAILLE_H, TAILLE_V));
			this.setSize(new java.awt.Dimension(TAILLE_H, TAILLE_V));

			addComponentListener(new ComponentAdapter() {
				public void componentMoved(ComponentEvent ce) {
					Preference.getIntance().setInt(Preference.P_X, getX());
					Preference.getIntance().setInt(Preference.P_Y, getY());
					Preference.getIntance().setInt(Preference.P_HAUTEUR, getHeight());
					Preference.getIntance().setInt(Preference.P_LARGEUR, getWidth());
					// Preference.getIntance().setBoolean(Preference.P_WINDOW_MAX,
					// false);
				}

				public void componentResized(ComponentEvent e) {
					Preference.getIntance().setInt(Preference.P_X, getX());
					Preference.getIntance().setInt(Preference.P_Y, getY());
					Preference.getIntance().setInt(Preference.P_HAUTEUR, getHeight());
					Preference.getIntance().setInt(Preference.P_LARGEUR, getWidth());
				}

			});

			addWindowStateListener(new WindowStateListener() {
				public void windowStateChanged(WindowEvent e) {
					Preference.getIntance().setBoolean(Preference.P_WINDOW_MAX, (e.getNewState() & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH);
				}
			});

			GestionnaireFavoris.getInstance(cahierOnglet);
			GestionnaireFavoris.getInstance().afficherTousLesFavoris();

			sortieTableau = jEditorPaneTableau.getDocument();
			sortieAudit = jEditorPaneAudit.getDocument();
			addWindowListener(this);

			// La feuille
			getJMenuEdition().add(undoAction).setIcon(Ressources.getImageIcon("edit-undo.png"));
			getJMenuEdition().add(redoAction).setIcon(Ressources.getImageIcon("edit-redo.png"));
			getJMenuEdition().addSeparator();

			// http://penserenjava.free.fr/pens_2.4/indexMaind0ce.html?chp=14&pge=10
			JMenuItem cut = new JMenuItem("Couper"), copy = new JMenuItem("Copier"), paste = new JMenuItem("Coller");

			couper = new CutL();
			copier = new CopyL();
			coller = new PasteL();

			cut.addActionListener(couper);
			copy.addActionListener(copier);
			paste.addActionListener(coller);

			copy.setIcon(Ressources.getImageIcon("edit-copy.png"));
			paste.setIcon(Ressources.getImageIcon("edit-paste.png"));
			cut.setIcon(Ressources.getImageIcon("edit-cut.png"));

			copy.setMnemonic(java.awt.event.KeyEvent.VK_P);
			paste.setMnemonic(java.awt.event.KeyEvent.VK_O);
			cut.setMnemonic(java.awt.event.KeyEvent.VK_C);

			getJMenuEdition().add(copy);
			getJMenuEdition().add(paste);
			getJMenuEdition().add(cut);
			getJMenuEdition().addSeparator();
			getJMenuEdition().add(getJMenuRechercher()).setIcon(Ressources.getImageIcon("format-indent-more.png"));

			getJMenuOutils().add(getJMenuFormater()).setIcon(Ressources.getImageIcon("format-indent-more.png"));
			getJMenuOutils().add(getJMenuLaToile()).setIcon(Ressources.getImageIcon("image-x-generic.png"));
			if (!Version.isPro()) {
				getJMenuOutils().addSeparator();
				getJMenuOutils().add(getJMenuManageur(Version.URL_GREFFONS)).setIcon(Ressources.getImageIcon("applications-other.png"));
			}
			getJMenuOutils().add(getJMenuGreffonLinotte()).setIcon(Ressources.getImageIcon("applications-other.png"));
			// getJMenuOutils().add(getJMenuConsole()).setIcon(Ressources.getImageIcon("utilities-terminal.png"));
			getJMenuOutils().addSeparator();
			JMenu options = new JMenu("Options");
			getJMenuOutils().add(options).setIcon(Ressources.getScaledImage(Ressources.getImageIcon("system-shutdown.png"), 24, 24));
			options.add(getJMenuThemes()).setIcon(Ressources.getImageIcon("themes.png"));
			options.add(getJMenuSaveWorkSpace()).setIcon(Ressources.getImageIcon("drive-harddisk.png"));

			options.add(getJMenuItemBonifieur()).setIcon(Ressources.getScaledImage(Ressources.getImageIcon("preferences-desktop-locale.png"), 24, 24));
			options.add(getJMenuManageurStyle()).setIcon(Ressources.getImageIcon("themes.png"));

			options.addSeparator();
			options.add(getJMenuDelaisDebogueur());

			jEditorPaneTableau.setOpaque(false);
			// jEditorPaneTableau.setForeground(COULEUR_TABLEAU);

			jEditorPaneTableau.setFont(font);

			// Popup pour effacer le tableau :
			popupTableau = new JPopupMenu();
			itemPopupTableau = new JMenuItem("Effacer le tableau");
			itemPopupTableau.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					effacerTableau();
				}

			});

			// Popup pour copier le tableau :
			itemPopupTableau2 = new JMenuItem("Copier le tableau");
			itemPopupTableau2.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					copierTableau();
				}

			});

			popupTableau.add(itemPopupTableau);
			popupTableau.add(itemPopupTableau2);
			MouseListener popupListener = new PopupListener(popupTableau);
			jEditorPaneTableau.addMouseListener(popupListener);

			final String sConstructionMenus = "Construction des menus de l'Atelier";

			splashWindow1.setProgressValue(7, sConstructionMenus);
			// construction du menu Verbe :

			threadLancementAtelier4 = new Thread() {
				public void run() {

					try {
						synchronized (this) {
							wait();
						}
					} catch (InterruptedException e) {
					}
					creationSousMenuVerbier(sConstructionMenus);

					try {
						for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
							jMenuThemes.add(new LFAction(info));
						}
					} catch (Exception e) {
					}

				}

			};
			threadLancementAtelier4.start();

			// A propos
			apropos = new Apropos(this);
			merci = new Merci(this);
			// Affichage des messages d'erreur
			Iterator<String> erreurs = RegistreDesActions.retourneErreurs();
			while (erreurs.hasNext()) {
				ecrireErreurTableau_(erreurs.next());
				ecrirelnTableau("");
			}
			RegistreDesActions.effaceErreurs();

			saut2ligne = false;
			// Ouvrir
			fileChooser_ouvrir = new JFileChooser(Ressources.getLocal());
			FiltreLivre filtre = new FiltreLivre(new String[] { "liv" }, "Livre Linotte (*.liv)");
			fileChooser_ouvrir.addChoosableFileFilter(filtre);
			fileChooser_ouvrir.setFileFilter(filtre);
			fileChooser_ouvrir.setDialogTitle("Ouvrir un livre Linotte");
			fileChooser_ouvrir.setApproveButtonText("Ouvrir le livre");
			// Ranger
			fileChooser_sauvegarder = new JFileChooser(Ressources.getLocal());
			FiltreLivre filtre2 = new FiltreLivre(new String[] { "liv" }, "Livre Linotte (*.liv)");
			fileChooser_sauvegarder.addChoosableFileFilter(filtre2);
			fileChooser_sauvegarder.setFileFilter(filtre2);
			fileChooser_sauvegarder.setDialogTitle("Ranger un livre Linotte");
			fileChooser_sauvegarder.setApproveButtonText("Ranger le livre");

			// Exporter PDF
			fileChooser_exporter = new JFileChooser(Ressources.getLocal());
			fileChooser_exporter.setSelectedFile(new File(Ressources.getLocal(), "Nouveau.pdf"));
			FiltreLivre filtre3 = new FiltreLivre(new String[] { "pdf" }, "Format PDF (*.pdf)");
			fileChooser_exporter.addChoosableFileFilter(filtre3);
			fileChooser_exporter.setFileFilter(filtre3);
			fileChooser_exporter.setDialogTitle("Exporter votre livre au format PDF");
			fileChooser_exporter.setApproveButtonText("Exporter le livre");

			// Exporter PNG
			fileChooser_exporterPNG = new JFileChooser(Ressources.getLocal());
			fileChooser_exporterPNG.setSelectedFile(new File(Ressources.getLocal(), "Nouveau.png"));
			FiltreLivre filtre4 = new FiltreLivre(new String[] { "png" }, "Format PNG (*.png)");
			fileChooser_exporterPNG.addChoosableFileFilter(filtre4);
			fileChooser_exporterPNG.setFileFilter(filtre4);
			fileChooser_exporterPNG.setDialogTitle("Exporter votre livre au format PNG");
			fileChooser_exporterPNG.setApproveButtonText("Exporter le livre");

			// Exporter HTML
			fileChooser_exporterHTML = new JFileChooser(Ressources.getLocal());
			fileChooser_exporterHTML.setSelectedFile(new File(Ressources.getLocal(), "Export.html"));
			FiltreLivre filtre5 = new FiltreLivre(new String[] { "html" }, "Format HTML (*.HTML)");
			fileChooser_exporterHTML.addChoosableFileFilter(filtre5);
			fileChooser_exporterHTML.setFileFilter(filtre5);
			fileChooser_exporterHTML.setDialogTitle("Exporter votre livre au format HTML");
			fileChooser_exporterHTML.setApproveButtonText("Exporter le livre");

			// Exporter RTF
			fileChooser_exporterRTF = new JFileChooser(Ressources.getLocal());
			fileChooser_exporterRTF.setSelectedFile(new File(Ressources.getLocal(), "Export.rtf"));
			FiltreLivre filtre6 = new FiltreLivre(new String[] { "rtf" }, "Format RTF (*.RTF)");
			fileChooser_exporterRTF.addChoosableFileFilter(filtre6);
			fileChooser_exporterRTF.setFileFilter(filtre6);
			fileChooser_exporterRTF.setDialogTitle("Exporter votre livre au format RTF");
			fileChooser_exporterRTF.setApproveButtonText("Exporter le livre");

			// Importer une greffon Java
			fileChooser_greffonJava = new JFileChooser(Ressources.getLocal());
			FiltreLivre filtre7 = new FiltreLivre(new String[] { "liv" }, "Livre Linotte (*.liv)");
			fileChooser_greffonJava.addChoosableFileFilter(filtre7);
			fileChooser_greffonJava.setFileFilter(filtre7);
			fileChooser_greffonJava.setDialogTitle("Ouvrir un livre Greffon Linotte");
			fileChooser_greffonJava.setApproveButtonText("Ouvrir le greffon Linotte");

			inspecteur = new Inspecteur(this);
			inspecteur.pack();

			this.setSize(TAILLE_H, TAILLE_V);
			this.setName("L'Atelier Linotte " + Version.getVersion());

			// splashWindow1.setProgressValue(8, "Chargement des livres");

			if (!Version.isBeta()) {
				// Vérification de la mise à jour :
				Updater.SuccessAction action = new UpdateSuccessAction();
				new Updater(Version.getVERSION_TECHNIQUE(), Version.getVERSION_TECHNIQUE_URL(), action);
			}
			getCahierCourant().effacerCahier();

			// Restauration des préférences :
			if (linotte.getLangage().isLegacy()) {
				affichagePresent = false;
				verbe = true;
			} else {
				affichagePresent = !verbe;
			}
			jMenuItemSaveWorkSpace.setSelected(saveWorkSpace);
			jMenuItemBonifieur.setSelected(bonifieur);
			jMenuItemDebogueur.setValue(delais_pas_a_pas);

			// Ouvrir le fichier :

			// Si premier lancement :
			if (!Preference.getIntance().isExiste()) {
				Preference.getIntance().setProperty(Preference.P_FICHIER + "_1",
						linotte.getLangage().getCheminExemple() + "/b_tutoriels/h_interfaces_utilisateur/Demonstration_IHM.liv");
				// Preference.getIntance().setProperty(Preference.P_FICHIER +
				// "_2",
				// "exemples/tutoriels/j_expert/messagerie_instantanee.liv");
				Preference.getIntance().setProperty(Preference.P_FICHIER + "_2",
						linotte.getLangage().getCheminExemple() + "/b_tutoriels/a_debutant/bienvenue.liv");
				// Pour les autres langages de programmation :
				Preference.getIntance().setProperty(Preference.P_FICHIER + "_3", linotte.getLangage().getCheminExemple() + "/a_debutant/bienvenue.liv");

			}

			File exemples = Ressources.getExemples(linotte.getLangage());
			if (exemples.isDirectory()) {
				// ecrirelnTableau("Retrouvez les exemples dans le répertoire : "
				// + exemples.getCanonicalPath());
			} else {
				// Patch sous linux si on utilise l'icone dans le menu pour
				// lancer l'Atelier :
				exemples = new File("/usr/share/langagelinotte/" + langage.getCheminExemple());
				if (exemples.isDirectory()) {
					// ecrirelnTableau("Retrouvez les exemples dans le répertoire : "
					// + exemples.getCanonicalPath());
				}
			}

			File temp = null;
			if (Preference.getIntance().getProperty(Preference.P_DIRECTORY) != null) {
				temp = new File(Preference.getIntance().getProperty(Preference.P_DIRECTORY));
			}
			if (temp == null && exemples.isDirectory()) {
				temp = exemples;
			}
			if (temp == null) {
				Ressources.getLocal();
			}
			fileChooser_ouvrir.setCurrentDirectory(temp);
			fileChooser_sauvegarder.setCurrentDirectory(temp);
			fileChooser_sauvegarder.setSelectedFile(new File(temp, "Nouveau.liv"));

			chargerFichiers();
			chargerHistorique();

			// Chargement de l'historique des fichiers ouverts :

			findAndReplace = new BoiteRecherche(Atelier.this);

			chargementDesClinottes();

			ecrirelnTableau("Prêt");

		} catch (Throwable e) {
			e.printStackTrace();
			Tools.showError(e);
			System.exit(-1);
		}
	}

	private void chargementDesClinottes() {
		// Ne doit pas s'exécuter :
		if (System.currentTimeMillis() < 1000) {
			new Thread("Chargement des ClipNottes") {
				public void run() {
					try {
						// Gestion des clipnottes
						// Execution des extensions :
						Parseur moteurXML = new Parseur();
						ParserContext atelierOutils = new ParserContext(MODE.GENERATION_RUNTIME);
						atelierOutils.linotte = linotte;
						File demo = new File(linotte.getLangage().getCheminExemple() + "/b_tutoriels/z_en_test/extension.liv");
						AKRuntime runtime = moteurXML.parseLivre(FichierOutils.lire(demo, new ArrayList<Integer>()), atelierOutils);
						ContextHelper.populate(runtime.getContext(), linotte, cahierCourant.getFichier(), null);
						RuntimeContext rcontext = (RuntimeContext) runtime.getContext();
						rcontext.setLibrairie(librairie.cloneMoi());
						runtime.execute();
					} catch (Exception e) {
						e.printStackTrace();
						ecrireErreurTableau("Impossible de charger le clipnotte : extension.liv");
					}
				}
			}.start();

			new Thread("Chargement des ClipNottes") {
				public void run() {
					try {
						// Gestion des clipnottes
						// Execution des extensions :
						Parseur moteurXML = new Parseur();
						ParserContext atelierOutils = new ParserContext(MODE.GENERATION_RUNTIME);
						atelierOutils.linotte = linotte;
						File demo = new File(linotte.getLangage().getCheminExemple() + "/b_tutoriels/z_en_test/clipnotte.liv");
						AKRuntime runtime = moteurXML.parseLivre(FichierOutils.lire(demo, new ArrayList<Integer>()), atelierOutils);
						ContextHelper.populate(runtime.getContext(), linotte, cahierCourant.getFichier(), null);
						RuntimeContext rcontext = (RuntimeContext) runtime.getContext();
						rcontext.setLibrairie(librairie.cloneMoi());
						runtime.execute();
					} catch (Exception e) {
						e.printStackTrace();
						ecrireErreurTableau("Impossible de charger le clipnotte : clipnotte.liv");
					}
				}
			}.start();

		}
	}

	private Langage chargementLangageProgrammation() {
		langageSwitch = new LangageSwitch(librairie, dialogframeihm);

		// Quelle langue doit être chargée au lancement de l'Atelier ?
		String parametreLangage = Preference.getIntance().getProperty(Preference.P_LANGAGE);
		if (parametreLangage == null) {
			parametreLangage = Langage.Linotte2.name();
			Preference.getIntance().setProperty(Preference.P_LANGAGE, parametreLangage);
		}
		Langage langage = null;
		try {
			langage = Langage.valueOf(parametreLangage);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (langage == null) {
			langage = Langage.Linotte2;
		}

		linotte = langageSwitch.selectionLangage(langage);
		return langage;
	}

	private void demarrageServeurHTTP() {
		// Lancement du serveur HTTP :
		try {
			int port_webonotte = 7777;

			if (0 != Preference.getIntance().getInt(Preference.P_WEBONOTTE_PORT)) {
				port_webonotte = Preference.getIntance().getInt(Preference.P_WEBONOTTE_PORT);
			}
			String root = Preference.getIntance().getWebRoot();
			if (null != Preference.getIntance().getProperty(Preference.P_WEBONOTTE_DIR)) {
				root = Preference.getIntance().getProperty(Preference.P_WEBONOTTE_DIR);
			}
			splashWindow1.setProgressValue(6, "Chargement du serveur Webonotte sur le port " + port_webonotte);
			Run.runStandEmbededServer("jetty", "Webonotte", librairie, port_webonotte, new File(root));
		} catch (Throwable e) {
			if (Version.isBeta())
				e.printStackTrace();
		}
	}

	private void chargerFichiers() {
		/*
		 * Chargement des fichiers sauvegardés :
		 */
		final List<String> fichiers = Preference.getIntance().getKeyFilter(Preference.P_FICHIER);
		/*
		 * On affiche dans l'ordre enregistré :
		 */
		Collections.sort(fichiers, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				try {
					return (new Integer(o1.substring(Preference.P_FICHIER.length() + 1)))
							.compareTo(new Integer(o2.substring(Preference.P_FICHIER.length() + 1)));
				} catch (Exception e) {
					return 0;
				}
			}
		});

		if (!fichiers.isEmpty()) {
			for (String f : fichiers) {
				File fichier = new File(Preference.getIntance().getProperty(f));
				// splashWindow1.setProgressValue(8,
				// "Chargement du livre : " + fichier.getName());
				if (fichier.exists()) {
					softCreerCahierCourant();
					try {
						getCahierCourant().chargerFichier(fichier);
						GestionnaireFavoris.getInstance().peuplerCahier(getCahierCourant());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			getCahierCourant().forceStyle();
		} else {
			// Initialisation de l'onglet vide :
			cahierOnglet.ajouterCahier(getCahierCourant());
		}
	}

	/**
	 * Chargement des fichiers historisés :
	 */
	private void chargerHistorique() {
		List<String> historiques = Preference.getIntance().getKeyFilter(Preference.P_HISTORIQUE);

		/**
		 * On affiche dans l'ordre enregistré :
		 */
		Collections.sort(historiques, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				try {
					return (new Integer(o1.substring(Preference.P_HISTORIQUE.length() + 1)))
							.compareTo(new Integer(o2.substring(Preference.P_HISTORIQUE.length() + 1)));
				} catch (Exception e) {
					return 0;
				}
			}
		});

		if (!historiques.isEmpty()) {
			for (String f : historiques) {
				File fichier = new File(Preference.getIntance().getProperty(f));
				ajouterHistorique(fichier.getAbsoluteFile(), false);
			}
			rechargerHistorique();
		}
	}

	/**
	 * Chargement des fichiers historisés :
	 */
	private void rechargerHistorique() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				jMenuHistorisque.removeAll();
				synchronized (listHistorique) {
					if (!listHistorique.isEmpty()) {
						for (OuvrirLivreAction f : listHistorique) {
							jMenuHistorisque.add(new JMenuItem(f), 0);
						}
					}
				}
			}
		});
	}

	/**
	 * This method initializes jPanelAtelier
	 * 
	 * @return JPanel
	 */
	private JPanel getJPanelAtelier() {
		if (jPanelAtelier == null) {
			jPanelAtelier = new JPanel();
			GridBagLayout gb = new GridBagLayout();

			GridBagConstraints gbc = new GridBagConstraints();
			jPanelAtelier.setLayout(gb);
			gbc.fill = GridBagConstraints.BOTH;
			gbc.weightx = 1;
			gbc.weighty = 1;
			gb.setConstraints(getJSplitPaneProjet(), gbc);
			jPanelAtelier.setPreferredSize(new java.awt.Dimension(600, 490));
			jPanelAtelier.add(getJSplitPaneProjet());
			jPanelAtelier.setComponentOrientation(java.awt.ComponentOrientation.LEFT_TO_RIGHT);
		}
		return jPanelAtelier;
	}

	public int taille_split_projet = 320;

	private JSplitPane getJSplitPaneProjet() {
		if (splitPaneProjet == null) {
			splitPaneProjet = new JSplitPane();
			if (Ressources.creationEDT()) {
				// JOptionPane.showMessageDialog(this,
				// "Votre espace de travail est créé dans le répertoire :\n" +
				// Ressources.getEDT());
			}

			try {
				// try si la librairie swingX n'est pas présente :
				explorateur = new ExplorateurProjet(FileSystemView.getFileSystemView(), Ressources.getGreffons(), this, Ressources.getEDT());
				splitPaneProjet.setLeftComponent(explorateur);
				splitPaneProjet.setRightComponent(getJSplitPaneAtelier());
				splitPaneProjet.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
				splitPaneProjet.setDividerSize(8);
				splitPaneProjet.setOneTouchExpandable(true);
				splitPaneProjet.addComponentListener(new ComponentListener() {
					public void componentHidden(ComponentEvent e) {
					}

					public void componentMoved(ComponentEvent e) {
					}

					public void componentResized(ComponentEvent e) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								splitPaneProjet.setDividerLocation(taille_split_projet);
							}
						});
					}

					public void componentShown(ComponentEvent e) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								splitPaneProjet.setDividerLocation(taille_split_projet);
							}
						});
					}
				});
			} catch (Throwable e) {
				e.printStackTrace();
				splitPaneProjet = getJSplitPaneAtelier();
			}
		}
		return splitPaneProjet;
	}

	private int taille_split_tableau = 400;

	private JSplitPane getJSplitPaneAtelier() {
		if (jSplitPaneAtelier == null) {
			jSplitPaneAtelier = new JSplitPane();
			jSplitPaneAtelier.setTopComponent(getJSplitPaneSommaire());

			tabbedPaneNavigateur = new JTabbedPane();
			tabbedPaneNavigateur.setTabPlacement(JTabbedPane.TOP);
			tabbedPaneNavigateur.addTab("Tableau", Ressources.getImageIcon("utilities-terminal.png"), getJScrollPaneTableau());
			tabbedPaneNavigateur.setMnemonicAt(0, KeyEvent.VK_T);
			int i = 1;
			tabbedPaneNavigateur.addTab("Audit", Ressources.getImageIcon("applications-development.png"), getJScrollPaneAudit());
			tabbedPaneNavigateur.setMnemonicAt(i, KeyEvent.VK_U);

			jSplitPaneAtelier.setBottomComponent(tabbedPaneNavigateur);
			jSplitPaneAtelier.setOrientation(JSplitPane.VERTICAL_SPLIT);
			jSplitPaneAtelier.setDividerSize(8);
			jSplitPaneAtelier.setOneTouchExpandable(true);
			jSplitPaneAtelier.addComponentListener(new ComponentListener() {
				public void componentHidden(ComponentEvent e) {
				}

				public void componentMoved(ComponentEvent e) {
				}

				public void componentResized(ComponentEvent e) {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							jSplitPaneAtelier.setDividerLocation(atelier.getHeight() - taille_split_tableau);
						}
					});
				}

				public void componentShown(ComponentEvent e) {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							jSplitPaneAtelier.setDividerLocation(atelier.getHeight() - taille_split_tableau);
						}
					});
				}
			});

		}
		return jSplitPaneAtelier;
	}

	private int taille_split_sommaire = 180;

	private JSplitPane getJSplitPaneSommaire() {

		if (splitPaneSommaire == null) {
			splitPaneSommaire = new JSplitPane();
			cahierOnglet = new Onglets();
			splitPaneSommaire.setLeftComponent(cahierOnglet);
			setSommaire(getCahierCourant().getJSommaire());
			splitPaneSommaire.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
			splitPaneSommaire.setDividerSize(8);
			splitPaneSommaire.setOneTouchExpandable(true);
			splitPaneSommaire.addComponentListener(new ComponentListener() {
				public void componentHidden(ComponentEvent e) {
				}

				public void componentMoved(ComponentEvent e) {
				}

				public void componentResized(ComponentEvent e) {
					refreshSommaireLocation();
				}

				public void componentShown(ComponentEvent e) {
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							splitPaneSommaire.setDividerLocation(splitPaneSommaire.getWidth() - taille_split_sommaire);
						}
					});
				}
			});
		}
		return splitPaneSommaire;
	}

	public void refreshSommaireLocation() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				splitPaneSommaire.setDividerLocation(splitPaneSommaire.getWidth() - taille_split_sommaire);
			}
		});
	}

	public void setSommaire(final JPanelSommaire sommaire) {
		final int location = splitPaneSommaire.getDividerLocation();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				splitPaneSommaire.setRightComponent(sommaire);
				splitPaneSommaire.setDividerLocation(location);
				if (cahierCourant.getEditorPanelCahier() != null)
					cahierCourant.getEditorPanelCahier().requestFocusInWindow();
			}
		});
	}

	protected JButton getJButtonUpdate() {
		if (jButtonUpdate == null) {
			jButtonUpdate = createSimpleButton(Ressources.getImageIcon("bandeau/software-update-available.png"));
			// jButtonUpdate.setToolTipText("Une nouvelle version de Linotte est disponible !");
			// jButtonUpdate.setFocusable(false);
			jButtonUpdate.setText("Nouvelle version disponible !");
			jButtonUpdate.setVisible(false);
			jButtonUpdate.setPressedIcon(Ressources.getImageIconePlusClaire("bandeau/software-update-available.png"));
			jButtonUpdate.setRolloverIcon(Ressources.getImageIconePlusClaire("bandeau/software-update-available.png"));
			jButtonUpdate.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
			jButtonUpdate.setVerticalTextPosition(buttonTextHorizontale);
			jButtonUpdate.setHorizontalTextPosition(buttonTextVerticale);
			jButtonUpdate.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e0) {
					try {
						Java6.getDesktop().browse(new URI(Version.getURL()));
					} catch (IOException e) {
						e.printStackTrace();
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}
				}
			});
		}
		return jButtonUpdate;
	}

	private static JButton createSimpleButton(String text) {
		JButton button = new JButton(text);
		button.setOpaque(false);
		button.setBorderPainted(false);
		button.setFocusPainted(false);
		button.setContentAreaFilled(false);
		return button;
	}

	private static JButton createSimpleButton(Icon icon) {
		JButton button = new JButton(icon);
		button.setOpaque(false);
		button.setBorderPainted(false);
		button.setFocusPainted(false);
		button.setContentAreaFilled(false);
		return button;
	}

	/**
	 * This method initializes jButtonLire
	 * 
	 * @return JButton
	 */
	public JButton getJButtonLire() {
		if (jButtonLire == null) {
			jButtonLire = createSimpleButton("Lire !");
			jButtonLire.setVerticalTextPosition(buttonTextHorizontale);
			jButtonLire.setHorizontalTextPosition(buttonTextVerticale);
			if (!Preference.getIntance().getBoolean(Preference.P_BULLE_AIDE_INACTIF))
				jButtonLire.setToolTipText("<html><b>Bouton Lire</b><br>" + "<i>Description</i> : Lis le livre présent dans le cahier<br>"
						+ "<i>Action</i> : Cliquer sur le bouton [Lire] ou appuyer sur les boutons Alt+L<br>" + "</html>");
			jButtonLire.setFocusable(false);
			jButtonLire.setIcon(Ressources.getImageIcon("bandeau/applications-multimedia.png"));
			jButtonLire.setPressedIcon(Ressources.getImageIconePlusClaire("bandeau/applications-multimedia.png"));
			jButtonLire.setRolloverIcon(Ressources.getImageIconePlusClaire("bandeau/applications-multimedia.png"));
			jButtonLire.setName("boutonLire");
			jButtonLire.setMnemonic(KeyEvent.VK_L);
			jButtonLire.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e0) {
					getCahierCourant().processStyle.setStopLecture();
					if (cahierOnglet.nbOnglet() > 0)
						FrameProcess.go(new StringBuilder(getCahierCourant().retourneTexteCahier() + "\n"), linotte, atelier, false, -1);
				}
			});
		}
		return jButtonLire;
	}

	/**
	 * This method initializes jButtonLire
	 * 
	 * @return JButton
	 */
	public JButton getJButtonTimbre() {
		if (jButtonTimbre == null) {
			jButtonTimbre = createSimpleButton("Voir code");
			jButtonTimbre.setVerticalTextPosition(buttonTextHorizontale);
			jButtonTimbre.setHorizontalTextPosition(buttonTextVerticale);
			if (!Preference.getIntance().getBoolean(Preference.P_BULLE_AIDE_INACTIF))
				jButtonTimbre.setToolTipText("<html><b>Bouton Blocs</b><br>" + "<i>Description</i> : Affiche le code du programme<br>"
						+ "<i>Action</i> : appuyer sur les boutons Alt+D<br>" + "</html>");
			jButtonTimbre.setFocusable(false);
			jButtonTimbre.setVisible(false);
			jButtonTimbre.setIcon(Ressources.getImageIcon("timbre/dev.png"));
			jButtonTimbre.setPressedIcon(Ressources.getImageIconePlusClaire("timbre/dev.png"));
			jButtonTimbre.setRolloverIcon(Ressources.getImageIconePlusClaire("timbre/dev.png"));
			jButtonTimbre.setName("boutonTimbre");
			jButtonTimbre.setMnemonic(KeyEvent.VK_C);
			jButtonTimbre.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					final Cahier cahier = getCahierCourant();
					cahier.setVisualiserTimbreVoirCode(!cahier.isVisualiserTimbreVoirCode());
					cahier.setVisualiserTimbre(true); // On force la changement
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							getCahierCourant().repaint();
							refreshBoutonTimbre(cahier);
						}
					});
				}
			});
		}
		return jButtonTimbre;
	}

	public void refreshBoutonTimbre(final Cahier cahier) {
		if (jButtonTimbre != null) {
			jButtonTimbre.setBorderPainted(cahier.isVisualiserTimbreVoirCode());
			jButtonTimbre.setContentAreaFilled(cahier.isVisualiserTimbreVoirCode());
		}
	}

	/**
	 * This method initializes jButtonLire
	 * 
	 * @return JButton
	 */
	public JButton getJButtonTester() {
		if (jButtonTester == null) {
			jButtonTester = new JButton();
			jButtonTester = createSimpleButton("Tester !");
			jButtonTester.setVisible(false);
			jButtonTester.setOpaque(true);
			jButtonTester.setBackground(Color.GREEN);
			jButtonTester.setVerticalTextPosition(buttonTextHorizontale);
			jButtonTester.setHorizontalTextPosition(buttonTextVerticale);
			if (!Preference.getIntance().getBoolean(Preference.P_BULLE_AIDE_INACTIF))
				jButtonTester.setToolTipText("<html><b>Bouton Tester</b><br>" + "<i>Description</i> : Teste le livre présent dans le cahier<br>"
						+ "<i>Action</i> : Cliquer sur le bouton [Tester] ou appuyer sur les boutons Alt+T<br>" + "</html>");
			jButtonTester.setFocusable(false);
			jButtonTester.setIcon(Ressources.getImageIcon("bandeau/start-here.png"));
			jButtonTester.setPressedIcon(Ressources.getImageIconePlusClaire("bandeau/start-here.png"));
			jButtonTester.setRolloverIcon(Ressources.getImageIconePlusClaire("bandeau/start-here.png"));
			jButtonTester.setName("boutonTester");
			jButtonTester.setMnemonic(KeyEvent.VK_T);
			jButtonTester.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e0) {
					getCahierCourant().processStyle.setStopLecture();
					if (cahierOnglet.nbOnglet() > 0)
						FrameProcess.go(new StringBuilder(getCahierCourant().retourneTexteCahier() + "\n"), linotte, atelier, true, -1);
				}
			});
		}
		return jButtonTester;
	}

	public JButton getJButtonPause() {
		if (jButtonPasAPas == null) {
			jButtonPasAPas = createSimpleButton(Debogueur.LIRE_AU_RALENTI);
			jButtonPasAPas.setVerticalTextPosition(buttonTextHorizontale);
			jButtonPasAPas.setHorizontalTextPosition(buttonTextVerticale);
			if (!Preference.getIntance().getBoolean(Preference.P_BULLE_AIDE_INACTIF))
				jButtonPasAPas.setToolTipText(Debogueur.TOOLTIP_BOUTON_RALENTI);
			jButtonPasAPas.setFocusable(false);
			jButtonPasAPas.setVisible(true);
			jButtonPasAPas.setIcon(Ressources.getImageIcon("bandeau/start-here.png"));
			jButtonPasAPas.setPressedIcon(Ressources.getImageIconePlusClaire("bandeau/start-here.png"));
			jButtonPasAPas.setRolloverIcon(Ressources.getImageIconePlusClaire("bandeau/start-here.png"));
			jButtonPasAPas.setName("boutonPause");
			jButtonPasAPas.setMnemonic(KeyEvent.VK_A);
			jButtonPasAPas.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e0) {
					if (jButtonLire.isEnabled()) { // très moche
						getCahierCourant().processStyle.setStopLecture();
						if (cahierOnglet.nbOnglet() > 0)
							FrameProcess.go(new StringBuilder(getCahierCourant().retourneTexteCahier() + "\n"), linotte, atelier, false,
									Preference.getIntance().getInt(Preference.P_PAS_A_PAS));
					} else {
						jButtonContinuer.setVisible(false);
						jButtonPasAPas.setVisible(false);
						FrameProcess.continuerDebogage();
					}
				}
			});
		}
		return jButtonPasAPas;
	}

	public JButton getJButtonContinuer() {
		if (jButtonContinuer == null) {
			jButtonContinuer = createSimpleButton("Continuer");
			jButtonContinuer.setVerticalTextPosition(buttonTextHorizontale);
			jButtonContinuer.setHorizontalTextPosition(buttonTextVerticale);
			jButtonContinuer.setToolTipText("Continuer (Alt+C)");
			jButtonContinuer.setFocusable(false);
			jButtonContinuer.setVisible(false);
			jButtonContinuer.setIcon(Ressources.getScaledImage(Ressources.getImageIcon("go-jump.png"), 32, 32));
			jButtonContinuer.setPressedIcon(Ressources.getScaledImage(Ressources.getImageIconePlusClaire("go-jump.png"), 32, 32));
			jButtonContinuer.setRolloverIcon(Ressources.getScaledImage(Ressources.getImageIconePlusClaire("go-jump.png"), 32, 32));
			jButtonContinuer.setName("boutonPause");
			jButtonContinuer.setMnemonic(KeyEvent.VK_C);
			jButtonContinuer.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e0) {
					jButtonContinuer.setVisible(false);
					jButtonPasAPas.setVisible(false);
					jButtonLire.setVisible(true);
					FrameProcess.stopDebogage();
				}
			});
		}
		return jButtonContinuer;
	}

	/**
	 * This method initializes jButtonStop
	 * 
	 * @return JButton
	 */
	public JButton getJButtonStop() {
		if (jButtonStop == null) {
			jButtonStop = createSimpleButton("Stop !");
			jButtonStop.setVerticalTextPosition(buttonTextHorizontale);
			jButtonStop.setHorizontalTextPosition(buttonTextVerticale);
			jButtonStop.setEnabled(false);
			jButtonStop.setFocusable(false);
			jButtonStop.setVisible(true);
			jButtonStop.setIcon(Ressources.getImageIcon("bandeau/process-stop.png"));
			jButtonStop.setPressedIcon(Ressources.getImageIconePlusClaire("bandeau/process-stop.png"));
			jButtonStop.setRolloverIcon(Ressources.getImageIconePlusClaire("bandeau/process-stop.png"));
			jButtonStop.setMnemonic(KeyEvent.VK_S);
			if (!Preference.getIntance().getBoolean(Preference.P_BULLE_AIDE_INACTIF))
				jButtonStop.setToolTipText("<html><b>Bouton Stop</b><br>" + "<i>Description</i> : Stop le livre en cours de lecture<br>"
						+ "<i>Action</i> : Cliquer sur le bouton [Stop] ou appuyer sur les boutons Alt+S<br>" + "</html>");
			jButtonStop.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					try {
						FrameProcess.stopProcess();
					} catch (StopException e1) {
					}
				}
			});
		}
		return jButtonStop;
	}

	protected JButton getJButtonLibrairie() {
		if (jButtonLibrairie == null) {
			jButtonLibrairie = createSimpleButton("Inspecteur...");
			jButtonLibrairie.setEnabled(true);
			jButtonLibrairie.setVerticalTextPosition(buttonTextHorizontale);
			jButtonLibrairie.setHorizontalTextPosition(buttonTextVerticale);
			jButtonLibrairie.setVerticalTextPosition(buttonTextHorizontale);
			jButtonLibrairie.setHorizontalTextPosition(buttonTextVerticale);
			jButtonLibrairie.setVisible(true);
			jButtonLibrairie.setFocusable(false);
			jButtonLibrairie.setIcon(Ressources.getImageIcon("bandeau/utilities-system-monitor.png"));
			jButtonLibrairie.setPressedIcon(Ressources.getImageIconePlusClaire("bandeau/utilities-system-monitor.png"));
			jButtonLibrairie.setRolloverIcon(Ressources.getImageIconePlusClaire("bandeau/utilities-system-monitor.png"));
			jButtonLibrairie.setMnemonic(KeyEvent.VK_P);
			jButtonLibrairie.setToolTipText("Inspecteur (Alt+P)");
			if (!Preference.getIntance().getBoolean(Preference.P_BULLE_AIDE_INACTIF))
				jButtonLibrairie.setToolTipText("<html><b>Bouton Inspecteur</b><br>" + "<i>Description</i> : Affiche les acteurs de la mémoire<br>"
						+ "<i>Action</i> : Cliquer sur le bouton [Inspecteur] ou appuyer sur les boutons Alt+P<br>" + "</html>");
			jButtonLibrairie.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					if (inspecteur.isVisible())
						inspecteur.setVisible(false);
					else
						inspecteur.setVisible(true);
					inspecteur.refresh(null);
				}
			});
		}
		return jButtonLibrairie;
	}

	protected JButton getJButtonRanger() {
		if (jButtonRanger == null) {
			jButtonRanger = createSimpleButton("Ranger");
			jButtonRanger.setVerticalTextPosition(buttonTextHorizontale);
			jButtonRanger.setHorizontalTextPosition(buttonTextVerticale);
			jButtonRanger.setEnabled(false);
			jButtonRanger.setFocusable(false);
			jButtonRanger.setVisible(true);
			jButtonRanger.setIcon(Ressources.getImageIcon("bandeau/document-save.png"));
			jButtonRanger.setPressedIcon(Ressources.getImageIconePlusClaire("bandeau/document-save.png"));
			jButtonRanger.setRolloverIcon(Ressources.getImageIconePlusClaire("bandeau/document-save.png"));
			jButtonRanger.setMnemonic(KeyEvent.VK_R);
			if (!Preference.getIntance().getBoolean(Preference.P_BULLE_AIDE_INACTIF))
				jButtonRanger.setToolTipText("<html><b>Bouton Ranger</b><br>" + "<i>Description</i> : Sauvegarder votre livre sur le disque<br>"
						+ "<i>Action</i> : Cliquer sur le bouton [Ranger] ou appuyer sur les boutons Alt+R<br>" + "</html>");
			jButtonRanger.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					try {
						if (getCahierCourant().getFichier() == null) {
							if (fileChooser_sauvegarder.showSaveDialog(FrameDialog.frame) == JFileChooser.APPROVE_OPTION) {
								getCahierCourant().sauvegarderFichier(fileChooser_sauvegarder.getSelectedFile());
							}
						} else {
							getCahierCourant().sauvegarderFichier(getCahierCourant().getFichier());
						}
					} catch (IOException e2) {
						JOptionPane.showMessageDialog(atelier,
								"Impossible de ranger le livre !\nVérifiez la présence de ce chemin et si vous avez les droits d'écriture dans ce répertoire :\n"
										+ getCahierCourant().getFichier().getParent(),
								"Rangement impossible", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
		return jButtonRanger;
	}

	public void ecrirelnTableau(String texte) {
		if (saut2ligne) {
			ecrireSimpleTableau("\n", null);
			saut2ligne = false;
		}
		ecrireSimpleTableau(texte + "\n", null);
		goFinTableau();
	}

	public void ecrireTableau(String texte) {
		if (saut2ligne) {
			ecrireSimpleTableau("\n", null);
			saut2ligne = false;
		}
		ecrireSimpleTableau(texte, null);
		goFinTableau();
	}

	public void ecrireErreurTableau(String texte) {
		dialogframeihm.getTableauBatch().ecrireErreur(texte);
	}

	public void ecrireErreurTableau_(String texte) {
		if (saut2ligne) {
			ecrireSimpleTableau("\n", null);
			saut2ligne = false;
		}
		ecrireSimpleTableau(texte, StyleLinotte.style_error);
		goFinTableau();
	}

	public void ecrireErreurTableau_(Object texte) {
		if (saut2ligne) {
			ecrireSimpleTableau("\n", null);
			saut2ligne = false;
		}
		ecrireSimpleTableau(texte, StyleLinotte.style_error);
		goFinTableau();
	}

	private void ecrireSimpleTableau(Object texte, AttributeSet attributeSet) {
		try {
			// Suppression du curseur.
			if (sortieTableau.getLength() > 0) {
				String carret = this.sortieTableau.getText(sortieTableau.getLength() - 1, 1);
				if (carret.equals("\u2588")) {
					this.sortieTableau.remove(sortieTableau.getLength() - 1, 1);
				}
			}
			if (texte instanceof Component)
				this.jEditorPaneTableau.insertComponent((Component) texte);
			else {
				this.sortieTableau.insertString(sortieTableau.getLength(), (String) texte + "", attributeSet);
				// Ajout du curseur.
				this.sortieTableau.insertString(sortieTableau.getLength(), "\u2588", null);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void effacerTableau() {
		synchronized (sortieTableau) {
			try {
				sortieTableau.remove(0, sortieTableau.getLength());
				// Ajout du curseur.
				this.sortieTableau.insertString(sortieTableau.getLength(), "\u2588", null);
				saut2ligne = false;
			} catch (BadLocationException e2) {
				e2.printStackTrace();
			}
		}
	}


	public void ecrireSimpleAudit(String texte, AttributeSet attributeSet) {
		try {
			this.sortieAudit.insertString(sortieAudit.getLength(), texte, attributeSet);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	public void ecrireDynamiquementCachier(String texte) {
		try {
			if (getJButtonLire().isEnabled())
				getCahierCourant().getDocumentCahier().insertString(getCahierCourant().getCaretPosition(), texte, null);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method initializes jScrollPaneAtelier
	 * 
	 * @return JScrollPane
	 */
	public Cahier getCahierCourant() {
		if (cahierCourant == null) {
			cahierCourant = new Cahier();
			cahierCourant.init(undoAction, redoAction, couper, copier, coller, font, this, linotte);
		}
		return cahierCourant;
	}

	public void setCahierCourant(Cahier panel) {
		cahierCourant = panel;
		setSommaire(panel.getJSommaire());
	}

	public Cahier creerCahierCourant(boolean type) {

		cahierCourant = new Cahier();
		cahierCourant.init(undoAction, redoAction, couper, copier, coller, font, this, linotte);
		setSommaire(cahierCourant.getJSommaire());
		cahierOnglet.ajouterCahier(cahierCourant);
		cahierCourant.setVisualiserTimbre(type);

		return cahierCourant;
	}

	public Cahier softCreerCahierCourant() {
		cahierCourant = new Cahier();
		cahierCourant.init(undoAction, redoAction, couper, copier, coller, font, this, linotte);
		cahierOnglet.ajouterCahier(cahierCourant);
		return cahierCourant;
	}

	/**
	 * This method initializes jScrollPaneTableau
	 * 
	 * @return JScrollPane
	 */
	private JComponent getJScrollPaneTableau() {

		JPanel tableau = new JPanel();
		tableau.setLayout(new BorderLayout());
		jScrollPaneTableau = new JScrollPane();
		JPanelBackGround background_panel = new JPanelBackGround(null, jScrollPaneTableau);
		background_panel.setLayout(new BorderLayout());
		background_panel.add(getJEditorPaneTableau());
		jScrollPaneTableau.getViewport().setView(background_panel);
		jScrollPaneTableau.setFocusable(false);
		jScrollPaneTableau.getVerticalScrollBar().setUnitIncrement(10);
		jScrollPaneTableau.getHorizontalScrollBar().setUnitIncrement(10);
		if (!Preference.getIntance().getBoolean(Preference.P_BULLE_AIDE_INACTIF))
			jScrollPaneTableau
					.setToolTipText("<html><b>Tableau</b><br>" + "<i>Description</i> : Affiche le résultat du livre ou les messages d'erreurs<br>" + "</html>");

		tableau.add(jScrollPaneTableau, BorderLayout.CENTER);
		tableau.add(getTeleType(), BorderLayout.SOUTH);
		return tableau;
	}

	int position = 0;

	/**
	 * @return
	 */
	private JPanel getTeleType() {
		// Prompteur :
		// pile des commandes
		final Stack<String> commandes = new Stack<String>();
		JPanel console = new JPanel();
		console.setLayout(new BorderLayout());
		final JXTextField entree = new JXTextField("Entrer une action à exécuter");
		final JButton action = new JButton("Essayer");
		action.setMnemonic(KeyEvent.VK_Y);
		if (!Preference.getIntance().getBoolean(Preference.P_BULLE_AIDE_INACTIF))
			entree.setToolTipText("<html><b>Télétype</b><br>" + "<i>Usage</i> : Une action Linotte<br>" + "<i>Exemple</i> : Affiche carré 3 + 12<br>"
					+ "<i>Action</i> : Touche [entrée] ou bouton [Essayer]<br>" + "</html>");
		if (!Preference.getIntance().getBoolean(Preference.P_BULLE_AIDE_INACTIF))
			action.setToolTipText("<html><b>Bouton Essayer</b><br>" + "<i>Description</i> : Exécute l'action entrée dans le télétype<br>"
					+ "<i>Action</i> : Cliquer sur le bouton [Apprécier]<br>" + "</html>");
		entree.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				action.doClick();
			}
		});
		entree.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {

			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					if (!commandes.isEmpty() && position < commandes.size() - 1) {
						position++;
						entree.setText(commandes.elementAt(commandes.size() - position - 1));
					}
				} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					if (!commandes.isEmpty() && position > 0) {
						position--;
						entree.setText(commandes.elementAt(commandes.size() - position - 1));
					} else {
						entree.setText("");
						position = -1;
					}
				}
			}
		});
		console.add(entree, BorderLayout.CENTER);
		action.setIcon(Ressources.getImageIcon("go-next.png"));
		action.setPressedIcon(Ressources.getImageIconePlusClaire("go-next.png"));
		action.setRolloverIcon(Ressources.getImageIconePlusClaire("go-next.png"));
		console.add(action, BorderLayout.EAST);
		action.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ConsoleProcess.go(new StringBuilder(entree.getText() + "\n"), linotte, atelier);
				if (entree.getText().trim().length() > 0) {
					commandes.remove(entree.getText());
					commandes.push(entree.getText());
				}
				entree.setText("");
				entree.grabFocus();
				position = -1;
				/*
				 * ByteArrayOutputStream st = new ByteArrayOutputStream();
				 * ObjectOutputStream oos; try { oos = new
				 * ObjectOutputStream(st);
				 * oos.writeObject(((Stack)commandes.clone())); oos.close(); }
				 * catch (IOException e1) { e1.printStackTrace(); }
				 * System.out.println(st.toString());
				 */
			}
		});
		return console;
	}

	/**
	 * Créé un paneau contenant un bouton pour l'audit, un bouton pour le trace,
	 * et un {@link MemoryMonitor}.
	 * 
	 * @return
	 */
	private JPanel getJScrollPaneAudit() {
		JPanel jPanel = new JPanel();
		jPanel.setLayout(new BorderLayout());
		if (jScrollPaneAudit == null) {
			jScrollPaneAudit = new JScrollPane();
			JPanelBackGround background_panel = new JPanelBackGround(null, jScrollPaneAudit);
			background_panel.setLayout(new BorderLayout());

			final JToggleButton button = new JToggleButton("Activer l'audit (peut ralentir l'Atelier...)", UIManager.getIcon("OptionPane.errorIcon"));
			button.setSelectedIcon(UIManager.getIcon("OptionPane.warningIcon"));
			button.setMnemonic(KeyEvent.VK_C);
			button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (!AKPatrol.active) {
						button.setSelected(true);
						AKPatrol.active = true;
					} else {
						button.setSelected(false);
						AKPatrol.active = false;
						effacerAudit();
					}
				}
			});

			final JToggleButton buttonTrace = new JToggleButton("Activer le fichier de trace .linotte/trace.log (peut ralentir l'Atelier...)",
					UIManager.getIcon("OptionPane.errorIcon"));

			buttonTrace.setSelectedIcon(UIManager.getIcon("OptionPane.warningIcon"));
			buttonTrace.setMnemonic(KeyEvent.VK_F);
			buttonTrace.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (!Trace.active) {
						buttonTrace.setSelected(true);
						Trace.active = true;
					} else {
						buttonTrace.setSelected(false);
						Trace.active = false;
					}
				}
			});
			JPanel admin = new JPanel();
			admin.add(button, BorderLayout.EAST);
			admin.add(buttonTrace, BorderLayout.WEST);
			background_panel.add(admin, BorderLayout.SOUTH);
			background_panel.add(getJEditorPaneAudit(), BorderLayout.CENTER);
			jScrollPaneAudit.getViewport().setView(background_panel);
			jScrollPaneAudit.setFocusable(false);
			jScrollPaneAudit.getVerticalScrollBar().setUnitIncrement(10);
			jScrollPaneAudit.getHorizontalScrollBar().setUnitIncrement(10);
		}
		jPanel.add(jScrollPaneAudit, BorderLayout.CENTER);
		MemoryMonitor memoryMonitor = new MemoryMonitor();
		jPanel.add(memoryMonitor, BorderLayout.EAST);
		memoryMonitor.surf.start();
		return jPanel;
	}

	/**
	 * Cette méthode initialise jEditorPaneTableau
	 * 
	 * @return JEditorPane
	 */
	private JEditorPane getJEditorPaneTableau() {
		if (jEditorPaneTableau == null) {
			DefaultStyledDocument temp = new DefaultStyledDocument();
			jEditorPaneTableau = new JTextPaneText(temp, false);
			jEditorPaneTableau.setEditable(false);
		}
		return jEditorPaneTableau;
	}

	private JEditorPane getJEditorPaneAudit() {
		if (jEditorPaneAudit == null) {
			DefaultStyledDocument temp = new DefaultStyledDocument();
			jEditorPaneAudit = new JTextPaneText(temp, false);
			jEditorPaneAudit.setEditable(false);
		}
		return jEditorPaneAudit;
	}

	private void goFinTableau() {
		jEditorPaneTableau.setCaretPosition(sortieTableau.getLength());
		tabbedPaneNavigateur.setSelectedIndex(0);
	}

	/**
	 * Initialise jMenuBar, la barre de menu.
	 * 
	 * @return JMenuBar
	 */
	private JMenuBar getxJMenuBar() {
		if (jMenuBar == null) {
			jMenuBar = new JMenuBar() {
				protected void paintComponent(Graphics g) {
					super.paintComponent(g);
					Color c1 = getBackground();
					Color c2 = getBackground().darker();
					((Graphics2D) g).setPaint(new GradientPaint(0, 0, c1, 500, 500, c2, false));
					Rectangle r = g.getClipBounds();
					g.fillRect(r.x, r.y, r.width, r.height);
				}
			};
			jMenuBar.setOpaque(false);
			int hauteurMenu = 80;
			jMenuBar.setMinimumSize(new Dimension(hauteurMenu, hauteurMenu));
			jMenuBar.setPreferredSize(new Dimension(hauteurMenu, hauteurMenu));
			jMenuBar.add(getJButtonLire());
			jMenuBar.add(getJButtonPause());
			jMenuBar.add(getJButtonContinuer());
			jMenuBar.add(getJButtonTester());
			jMenuBar.add(getJButtonStop());
			jMenuBar.add(getJButtonLibrairie());
			jMenuBar.add(creationSeparator());
			jMenuBar.add(getJButtonRanger());
			jMenuBar.add(getJMenuBibliotheque());
			jMenuBar.add(creationSeparator());
			jMenuBar.add(getJMenuEdition());
			jMenuBar.add(getJMenuOutils());
			jMenuBar.add(creationSeparator());
			jMenuBar.add(getJMenuVerbier());
			jMenuBar.add(getJButtonTimbre());
			jMenuBar.add(creationSeparator());
			jMenuBar.add(getJMenuLangage());
			jMenuBar.add(Box.createHorizontalGlue());
			jMenuBar.add(getJButtonUpdate());
			if (Version.isBeta()) {
				jMenuBar.add(getTexteBeta());
				jMenuBar.add(Box.createHorizontalStrut(30));
			}
			jMenuBar.add(Box.createHorizontalStrut(30));
			jMenuBar.add(getJMenuLinotte());
		}
		return jMenuBar;
	}

	private JSeparator creationSeparator() {
		JSeparator js = new JSeparator(JSeparator.VERTICAL);
		js.setMaximumSize(new Dimension(10, 60));
		return js;
	}

	private JButton getTexteBeta() {
		JButton jButtonBeta;
		jButtonBeta = createSimpleButton("Version de travail " + Version.getVersion());
		jButtonBeta.setVerticalTextPosition(buttonTextHorizontale);
		jButtonBeta.setHorizontalTextPosition(buttonTextVerticale);
		jButtonBeta.setToolTipText("<html><b>Version de travail</b><br>" + "Vous utilisez la version de travail " + Version.getVersion()
				+ " de l'Atelier Linotte<br>" + "Merci d'informer l'auteur des bogues que vous pourriez rencontrer !<br>" + "</html>");
		jButtonBeta.setFocusable(false);
		jButtonBeta.setIcon(Ressources.getImageIcon("face-devilish.png"));
		jButtonBeta.setPressedIcon(Ressources.getImageIconePlusClaire("face-devilish.png"));
		jButtonBeta.setRolloverIcon(Ressources.getImageIconePlusClaire("face-devilish.png"));
		jButtonBeta.setMnemonic(KeyEvent.VK_B);
		jButtonBeta.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e0) {
				try {
					final URI forum = new URI("http://langagelinotte.free.fr/");
					Java6.getDesktop().browse(forum);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		return jButtonBeta;
	}

	/**
	 * This method initializes jMenuEdition
	 * 
	 * @return JMenu
	 */
	private JMenu getJMenuEdition() {
		if (jMenuEdition == null) {

			jMenuEdition = createJMenuNonOpaque("    Edition    ", Ressources.getImageIconePlusClaire("bandeau/edit-select-all.png"),
					Ressources.getImageIcon("bandeau/edit-select-all.png"));
			jMenuEdition.setVerticalTextPosition(buttonTextHorizontale);
			jMenuEdition.setHorizontalTextPosition(buttonTextVerticale);
			jMenuEdition.setMnemonic(KeyEvent.VK_E);
		}
		return jMenuEdition;
	}

	private JMenu getJMenuOutils() {
		if (jMenuOutils == null) {
			jMenuOutils = createJMenuNonOpaque("    Outils    ", Ressources.getImageIconePlusClaire("bandeau/forum_openoffice.png"),
					Ressources.getImageIcon("bandeau/forum_openoffice.png"));
			jMenuOutils.setVerticalTextPosition(buttonTextHorizontale);
			jMenuOutils.setHorizontalTextPosition(buttonTextVerticale);
			jMenuOutils.setMnemonic(KeyEvent.VK_I);
		}
		return jMenuOutils;
	}

	/**
	 * This method initializes jMenuBibliotheque
	 * 
	 * @return JMenu
	 */
	private JMenu getJMenuBibliotheque() {
		if (jMenuBibliotheque == null) {
			jMenuBibliotheque = createJMenuNonOpaque("Bibliothèques", Ressources.getImageIconePlusClaire("bandeau/system-file-manager.png"),
					Ressources.getImageIcon("bandeau/system-file-manager.png"));
			jMenuBibliotheque.setVerticalTextPosition(buttonTextHorizontale);
			jMenuBibliotheque.setHorizontalTextPosition(buttonTextVerticale);
			jMenuBibliotheque.add(getJMenuItemNouveau()).setIcon(Ressources.getImageIcon("document-new.png"));
			jMenuBibliotheque.add(getJMenuItemNouveauLivreGraphique()).setIcon(Ressources.getImageIcon("timbre/timbre.png"));
			jMenuBibliotheque.addSeparator();
			jMenuBibliotheque.add(getJMenuItemDisque()).setIcon(Ressources.getImageIcon("document-open.png"));
			jMenuBibliotheque.add(getJMenuItemRangerSous()).setIcon(Ressources.getImageIcon("drive-harddisk.png"));
			jMenuBibliotheque.addSeparator();
			jMenuBibliotheque.add(getJMenuItemExporter()).setIcon(Ressources.getImageIcon("pdf_document.png"));
			jMenuBibliotheque.add(getJMenuItemExporterHTML()).setIcon(Ressources.getImageIcon("edit-copy.png"));
			jMenuBibliotheque.add(getJMenuItemExporterRTF()).setIcon(Ressources.getImageIcon("edit-copy.png"));
			jMenuBibliotheque.add(getJMenuItemExporterPNG()).setIcon(Ressources.getImageIcon("image-x-generic.png"));

			jMenuBibliotheque.addSeparator();
			// Ajout de l'historique des derniers livres ouverts
			jMenuBibliotheque.add(getJMenuItemHistorique()).setIcon(Ressources.getImageIcon("format-indent-more.png"));
			jMenuBibliotheque.setActionCommand("Lire");
			jMenuBibliotheque.setMnemonic(KeyEvent.VK_B);
		}
		return jMenuBibliotheque;
	}

	public JMenu getJMenuLangage() {
		if (jMenuLangage == null) {
			jMenuLangage = createJMenuNonOpaque("??", Ressources.getImageIconePlusClaire("bandeau/typography.png"),
					Ressources.getImageIcon("bandeau/typography.png"));
			jMenuLangage.setVerticalTextPosition(buttonTextHorizontale);
			jMenuLangage.setHorizontalTextPosition(buttonTextVerticale);
			jMenuLangage.setMnemonic(KeyEvent.VK_G);
			groupCheckBoxLangage = new ButtonGroup();
			for (Langage langage : Langage.values()) {
				groupCheckBoxLangage.add(jMenuLangage.add(getJMenuLangageProgrammation(langage)));
			}

		}
		return jMenuLangage;
	}

	private JMenuItem getJMenuLangageProgrammation(final Langage langage) {
		JMenuItem jMenuItemLangageLinnet = new JRadioButtonMenuItem();
		jMenuItemLangageLinnet.setText("Programmer avec le langage " + langage.getNom());
		if (langage == linotte.getLangage()) {
			jMenuItemLangageLinnet.setSelected(true);
			jMenuLangage.setText("Langage " + langage.getNom());
		}
		jMenuItemLangageLinnet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				linotte = langageSwitch.selectionLangage(langage);
				getToile().setInterpreteur(linotte);
				getCahierOnglet().forceMiseAJourStyle();
				getJMenuVerbier().removeAll();
				jMenuCondition = null;
				jMenuBoucle = null;
				jMenuEspeces = null;
				jMenuMathematiques = null;
				verbes_imperatif.clear();
				creationSousMenuVerbier("Rechargement du menu");
				Preference.getIntance().setProperty(Preference.P_LANGAGE, langage.name());
				jMenuLangage.setText("Langage " + langage.getNom());
				explorateur.changeLangage(FileSystemView.getFileSystemView(), Atelier.this);
				JOptionPane.showMessageDialog(atelier,
						"Bravo, vous pouvez programmer en langage " + langage.getNom() + " !"
								+ "\nA gauche, les exemples du tutoriel correspondent maintenant à ce langage.",
						"Changement de langage de programmation", JOptionPane.PLAIN_MESSAGE, Ressources.getImageIcon("extension.png"));
				getCahierCourant().changeAtelierEtat();
			}
		});
		return jMenuItemLangageLinnet;
	}

	private JMenu getJMenuVerbier() {
		if (jMenuVerbier == null) {
			jMenuVerbier = createJMenuNonOpaque("  Le verbier  ", Ressources.getImageIconePlusClaire("bandeau/compass.png"),
					Ressources.getImageIcon("bandeau/compass.png"));
			VerticalGridLayout menuGrid = new VerticalGridLayout(30, 0);
			jMenuVerbier.getPopupMenu().setLayout(menuGrid);
			jMenuVerbier.setActionCommand("Le verbier");
			jMenuVerbier.setVerticalTextPosition(buttonTextHorizontale);
			jMenuVerbier.setHorizontalTextPosition(buttonTextVerticale);
			jMenuVerbier.setMnemonic(KeyEvent.VK_V);
		}
		return jMenuVerbier;
	}

	private JMenuItem getJMenuCondition() {
		if (jMenuCondition == null) {
			jMenuCondition = new JMenu();
			jMenuCondition.setActionCommand("Conditions");
			jMenuCondition.setText("Conditions");
			jMenuCondition.setBackground(menuBarColor);
			jMenuCondition.setForeground(textBarColor);
			jMenuCondition.setBorderPainted(menuBarBordure);
			jMenuCondition.setIcon(Ressources.getImageIcon("system-shutdown.png"));
		}
		return jMenuCondition;
	}

	private JMenuItem getJMenuMathematiques() {
		if (jMenuMathematiques == null) {
			jMenuMathematiques = new JMenu();
			jMenuMathematiques.setActionCommand("Mathématiques");
			jMenuMathematiques.setText("Mathématiques");
			jMenuMathematiques.setBackground(menuBarColor);
			jMenuMathematiques.setForeground(textBarColor);
			jMenuMathematiques.setBorderPainted(menuBarBordure);
			jMenuMathematiques.setIcon(Ressources.getImageIcon("system-shutdown.png"));
		}
		return jMenuMathematiques;
	}

	private JMenuItem getJMenuBoucle() {
		if (jMenuBoucle == null) {
			jMenuBoucle = new JMenu();
			jMenuBoucle.setActionCommand("Boucles");
			jMenuBoucle.setText("Boucles");
			jMenuBoucle.setBackground(menuBarColor);
			jMenuBoucle.setForeground(textBarColor);
			jMenuBoucle.setBorderPainted(menuBarBordure);
			jMenuBoucle.setIcon(Ressources.getImageIcon("system-shutdown.png"));
		}
		return jMenuBoucle;
	}

	private JMenuItem getJMenuEspeces() {
		if (jMenuEspeces == null) {
			jMenuEspeces = new JMenu();
			VerticalGridLayout menuGrid = new VerticalGridLayout(30, 0);
			jMenuEspeces.getPopupMenu().setLayout(menuGrid);
			jMenuEspeces.setActionCommand("Espèces");
			jMenuEspeces.setText("Espèces");
			jMenuEspeces.setBackground(menuBarColor);
			jMenuEspeces.setForeground(textBarColor);
			Border border = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Espèces");
			jMenuEspeces.getPopupMenu().setBorder(border);
			jMenuEspeces.setIcon(Ressources.getImageIcon("system-shutdown.png"));
		}
		return jMenuEspeces;
	}

	private JMenuItem getJMenuThemes() {
		if (jMenuThemes == null) {
			jMenuThemes = new JMenu();
			jMenuThemes.setActionCommand("Habillages");
			jMenuThemes.setMnemonic(KeyEvent.VK_T);
			jMenuThemes.setText("Habillages");
			jMenuThemes.setBackground(menuBarColor);
			jMenuThemes.setForeground(textBarColor);
			jMenuThemes.setBorderPainted(menuBarBordure);
			jMenuThemes.setIcon(Ressources.getImageIcon("themes.png"));

		}
		return jMenuThemes;
	}

	public class OuvrirLivreAction extends AbstractAction {

		private File info;

		public OuvrirLivreAction(File info) {
			super(info.getName());
			this.info = info;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				synchronized (listHistorique) {
					listHistorique.remove(this);
				}
				ouvrirLivre(info);
			} catch (Exception e1) {
			}
		}
	}

	/**
	 * Action pour changer le Look & Feel Swing.
	 */
	public class LFAction extends AbstractAction {

		private LookAndFeelInfo info;

		public LFAction(LookAndFeelInfo info) {
			super("Habillage " + info.getName(), Ressources.getImageIcon("preferences-desktop-locale.png"));
			this.info = info;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				UIManager.setLookAndFeel(info.getClassName());
				SwingUtilities.updateComponentTreeUI(Atelier.this);
				Preference.getIntance().setProperty(Preference.P_STYLE, UIManager.getLookAndFeel().getClass().getName());
			} catch (Exception eX) {
				eX.printStackTrace();
			}
		}
	}

	private JMenu getJMenuCouleurs(int m) {
		JMenu jMenuCouleurs = new JMenu();
		jMenuCouleurs.setActionCommand("Couleurs");
		jMenuCouleurs.setText("Couleurs " + m);
		jMenuCouleurs.setBackground(menuBarColor);
		jMenuCouleurs.setForeground(textBarColor);
		jMenuCouleurs.setBorderPainted(menuBarBordure);
		jMenuCouleurs.setIcon(Ressources.getScaledImage(Ressources.getImageIcon("preferences-desktop-locale.png"), 16, 16));
		return jMenuCouleurs;
	}

	/**
	 * This method initializes jMenuItemDisque
	 * 
	 * @return JMenuItem
	 */
	private JMenuItem getJMenuItemDisque() {
		if (jMenuItemDisque == null) {
			jMenuItemDisque = new JMenuItem();
			jMenuItemDisque.setText("Ouvrir un livre...");
			jMenuItemDisque.setMnemonic(java.awt.event.KeyEvent.VK_O);
			jMenuItemDisque.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// if
					// (verifierLivre("Voulez-vous vraiment ouvrir un autre livre ?"))
					// {
					int returnVal = fileChooser_ouvrir.showOpenDialog(FrameDialog.frame);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						try {
							ouvrirLivre(new File(fileChooser_ouvrir.getSelectedFile().getAbsolutePath()));
						} catch (Exception e1) {
						}
					}
				}
			});
		}
		return jMenuItemDisque;
	}

	public void setLivreFromDragNDrop(File livre) {
		if (verifierLivre("Voulez-vous vraiment ouvrir un autre livre ?")) {
			try {
				ouvrirLivre(livre);
				// creerCahierCourant();
				// getCahierCourant().chargerFichier(livre);
			} catch (Exception e1) {
			}
		}
	}

	// getJMenuItemNouveau
	private JMenuItem getJMenuItemNouveau() {
		if (jMenuItemNouveau == null) {
			jMenuItemNouveau = new JMenuItem();
			jMenuItemNouveau.setText("Nouveau livre");
			jMenuItemNouveau.setMnemonic(java.awt.event.KeyEvent.VK_N);
			jMenuItemNouveau.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					creerCahierCourant(false);
				}
			});
		}
		return jMenuItemNouveau;
	}

	// getJMenuItemNouveau
	private JMenuItem getJMenuItemNouveauLivreGraphique() {
		if (jMenuItemNouveauLivreGraphique == null) {
			jMenuItemNouveauLivreGraphique = new JMenuItem();
			jMenuItemNouveauLivreGraphique.setText("Nouveau livre visuel");
			jMenuItemNouveauLivreGraphique.setMnemonic(java.awt.event.KeyEvent.VK_G);
			jMenuItemNouveauLivreGraphique.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					creerCahierCourant(true);
				}
			});
		}
		return jMenuItemNouveauLivreGraphique;
	}

	private JMenuItem getJMenuItemRangerSous() {
		if (jMenuItemRangerSous == null) {
			jMenuItemRangerSous = new JMenuItem();
			jMenuItemRangerSous.setText("Ranger sous...");
			jMenuItemRangerSous.setMnemonic(java.awt.event.KeyEvent.VK_S);
			jMenuItemRangerSous.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						if (fileChooser_sauvegarder.showSaveDialog(FrameDialog.frame) == JFileChooser.APPROVE_OPTION) {
							if (fileChooser_sauvegarder.getSelectedFile() != null)
								getCahierCourant().sauvegarderFichier(fileChooser_sauvegarder.getSelectedFile());
						}
					} catch (IOException e2) {
						JOptionPane.showMessageDialog(atelier,
								"Impossible de ranger le livre !\nVérifiez la présence de ce chemin et si vous avez les droits d'écriture dans ce répertoire :\n"
										+ getCahierCourant().getFichier().getParent(),
								"Rangement impossible", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
		return jMenuItemRangerSous;
	}

	private JMenuItem getJMenuItemHistorique() {
		jMenuHistorisque = new JMenu("Derniers livres ouverts");
		return jMenuHistorisque;
	}

	public JMenuItem getJMenuItemExporter() {
		if (jMenuItemExporter == null) {
			jMenuItemExporter = new JMenuItem();
			jMenuItemExporter.setText("Exporter au format PDF...");
			jMenuItemExporter.setMnemonic(java.awt.event.KeyEvent.VK_T);
			jMenuItemExporter.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						if (fileChooser_exporter.showSaveDialog(FrameDialog.frame) == JFileChooser.APPROVE_OPTION) {
							FileOutputStream fos = new FileOutputStream(fileChooser_exporter.getSelectedFile());
							JTextPaneToPdf.paintToPDF(getCahierCourant().getEditorPanelCahier(), fos, "L'Atelier Linotte " + Version.getVersion());
						}
					} catch (Exception e2) {
						e2.printStackTrace();
						JOptionPane.showMessageDialog(atelier, "Impossible d'exporter le livre !", "Rangement impossible", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
			jMenuItemExporter.setVisible(JTextPaneToPdf.can());
		}
		return jMenuItemExporter;
	}

	public JMenuItem getJMenuItemExporterHTML() {
		if (jMenuItemExporterHTML == null) {
			jMenuItemExporterHTML = new JMenuItem();
			jMenuItemExporterHTML.setText("Exporter au format HTML");
			jMenuItemExporterHTML.setMnemonic(java.awt.event.KeyEvent.VK_H);
			jMenuItemExporterHTML.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						if (fileChooser_exporterHTML.showSaveDialog(FrameDialog.frame) == JFileChooser.APPROVE_OPTION) {
							FileOutputStream fos = new FileOutputStream(fileChooser_exporterHTML.getSelectedFile());
							getCahierCourant().scroller();
							Thread.sleep(1000);
							byte[] extractionCahier = extractionCahier(new HTMLEditorKit());
							String flux = new String(extractionCahier);
							flux = flux.replaceAll("\t", "&nbsp;&nbsp;");
							fos.write(flux.getBytes());
							fos.flush();
							fos.close();
						}
					} catch (Exception e2) {
						e2.printStackTrace();
						JOptionPane.showMessageDialog(atelier, "Impossible d'exporter le livre !", "Rangement impossible", JOptionPane.ERROR_MESSAGE);
					}

				}
			});
		}
		return jMenuItemExporterHTML;
	}

	public JMenuItem getJMenuItemExporterRTF() {
		if (jMenuItemExporterRTF == null) {
			jMenuItemExporterRTF = new JMenuItem();
			jMenuItemExporterRTF.setText("Exporter au format RTF ( Rich Text Format )");
			jMenuItemExporterRTF.setMnemonic(java.awt.event.KeyEvent.VK_R);
			jMenuItemExporterRTF.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						if (fileChooser_exporterRTF.showSaveDialog(FrameDialog.frame) == JFileChooser.APPROVE_OPTION) {
							FileOutputStream fos = new FileOutputStream(fileChooser_exporterRTF.getSelectedFile());
							getCahierCourant().scroller();
							fos.write(extractionCahier(new RTFEditorKit()));
							fos.flush();
							fos.close();
						}
					} catch (Exception e2) {
						e2.printStackTrace();
						JOptionPane.showMessageDialog(atelier, "Impossible d'exporter le livre !", "Rangement impossible", JOptionPane.ERROR_MESSAGE);
					}

				}
			});
		}
		return jMenuItemExporterRTF;
	}

	private byte[] extractionCahier(EditorKit editorKit) {
		ByteArrayOutputStream str = new ByteArrayOutputStream();
		try {
			editorKit.write(str, getCahierCourant().getDocumentCahier(), 0, getCahierCourant().getDocumentCahier().getLength());
		} catch (Exception ex) {
		}
		return str.toByteArray();
	}

	public JMenuItem getJMenuItemExporterPNG() {
		if (jMenuItemExporterPNG == null) {
			jMenuItemExporterPNG = new JMenuItem();
			jMenuItemExporterPNG.setText("Exporter au format PNG...");
			jMenuItemExporterPNG.setMnemonic(java.awt.event.KeyEvent.VK_G);
			jMenuItemExporterPNG.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						if (fileChooser_exporterPNG.showSaveDialog(FrameDialog.frame) == JFileChooser.APPROVE_OPTION) {
							FileOutputStream fos = new FileOutputStream(fileChooser_exporterPNG.getSelectedFile());
							BufferedImage bi = new BufferedImage(getCahierCourant().getEditorPanelCahier().getWidth(),
									getCahierCourant().getEditorPanelCahier().getHeight(), BufferedImage.TYPE_INT_RGB);
							Graphics2D g2 = bi.createGraphics();
							g2.setColor(Color.WHITE);
							g2.fillRect(0, 0, bi.getWidth(), bi.getHeight());
							getCahierCourant().getEditorPanelCahier().print(g2);
							g2.dispose();
							try {
								ImageIO.write(bi, "png", fos);
							} catch (IOException ioe) {
								ioe.printStackTrace(System.err);
							}
							fos.close();
						}
					} catch (Exception e2) {
						e2.printStackTrace();
						JOptionPane.showMessageDialog(atelier, "Impossible d'exporter le livre !", "Rangement impossible", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
		return jMenuItemExporterPNG;
	}

	private JMenuItem getJMenuItemPresent() {
		if (jMenuItemPresent == null) {
			jMenuItemPresent = new JRadioButtonMenuItem();
			jMenuItemPresent.setText("Présent");
			jMenuItemPresent.setSelected(true);
			jMenuItemPresent.setIcon(Ressources.getImageIcon("system-users.png"));
			jMenuItemPresent.setMnemonic(java.awt.event.KeyEvent.VK_P);
			jMenuItemPresent.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					affichagePresent = false;
					Preference.getIntance().setBoolean(Preference.P_MODE_VERBE, affichagePresent);
				}
			});
		}
		return jMenuItemPresent;
	}

	/**
	 * Une icone de lien symbolique
	 */
	private final ImageIcon iconeLien = Ressources.getImageIcon("emblem-symbolic-link.png");

	private final ImageIcon iconeEditeurTexte = Ressources.getImageIcon("accessories-text-editor.png");

	private String capitalizeFirstLetter(String value) {
		if (value == null) {
			return null;
		}
		if (value.length() == 0) {
			return value;
		}
		StringBuilder result = new StringBuilder(value);
		result.replace(0, 1, result.substring(0, 1).toUpperCase());
		return result.toString();
	}

	private class JMenuItemID extends JMenuItem {
		String id;

		public JMenuItemID(String texte, String pid) {
			super(texte);
			id = pid;
		}

		public String getID() {
			return id;
		}
	}

	/**
	 * TODO: Méthode à réécrire.... très moche !
	 * 
	 * @param verbe
	 * @param id
	 */
	private void creerJMenuVerbe(String verbe, String id) {
		verbe = capitalizeFirstLetter(verbe);

		JMenuItemID itemPresent = new JMenuItemID(verbe, id);

		if (!verbes_icone.get(id)) {
			itemPresent.setIcon(iconeLien);
		} else {
			itemPresent.setIcon(iconeEditeurTexte);
		}

		itemPresent.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				if (affichagePresent) {
					getCahierCourant().ecrireInsertAuCurseurCachier(verbes_present.get(((JMenuItemID) e.getSource()).getID()));
				} else {
					getCahierCourant().ecrireInsertAuCurseurCachier(verbes_imperatif.get(((JMenuItemID) e.getSource()).getID()));
				}
			}
		});
		if (verbes_conditions.get(id) == null) {
			if (verbes_especes.get(id) != null) {
				if (slots_prototype.get(id) != null) {
					JMenuItem menu = creerMenu(verbe + "...");
					jMenuEspeces.add(menu);
					menu.add(itemPresent);
					for (final String s : slots_prototype.get(id)) {
						JMenuItem fils = new JMenuItem("." + s);
						if (!verbes_icone.get(id)) {
							fils.setIcon(iconeLien);
						} else {
							fils.setIcon(iconeEditeurTexte);
						}
						menu.add(fils);
						fils.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								getCahierCourant().ecrireInsertAuCurseurCachier("." + s);
							}
						});
					}
				} else {
					jMenuEspeces.add(itemPresent);
				}
			} else if (verbes_mathematiques.get(id) == null) {
				if (verbes_boucle.get(id) == null)
					jMenuVerbier.add(itemPresent);
				else
					jMenuBoucle.add(itemPresent);
			} else {
				jMenuMathematiques.add(itemPresent);
			}
		} else {
			jMenuCondition.add(itemPresent);
		}
		if (verbes_aide.get(id) != null) {
			itemPresent.setToolTipText(verbes_aide.get(id));
		}
	}

	private JMenuItem getJMenuItemImperatif() {
		if (jMenuItemImperatif == null) {
			jMenuItemImperatif = new JRadioButtonMenuItem();
			jMenuItemImperatif.setText("Impératif");
			jMenuItemImperatif.setSelected(true);
			jMenuItemImperatif.setIcon(Ressources.getImageIcon("computer.png"));
			jMenuItemImperatif.setMnemonic(java.awt.event.KeyEvent.VK_I);
			jMenuItemImperatif.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					affichagePresent = true;
					Preference.getIntance().setBoolean(Preference.P_MODE_VERBE, affichagePresent);
				}
			});
		}
		return jMenuItemImperatif;
	}

	private JMenuItem getJMenuSaveWorkSpace() {
		if (jMenuItemSaveWorkSpace == null) {
			jMenuItemSaveWorkSpace = new JCheckBoxMenuItem();
			jMenuItemSaveWorkSpace.setText("Mémoriser les livres");
			jMenuItemSaveWorkSpace.setSelected(false);
			jMenuItemSaveWorkSpace.setMnemonic(java.awt.event.KeyEvent.VK_M);
			jMenuItemSaveWorkSpace.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Preference.getIntance().setBoolean(Preference.P_MODE_SAVE_WORKSPACE, jMenuItemSaveWorkSpace.isSelected());
				}
			});
		}
		return jMenuItemSaveWorkSpace;
	}

	private JMenuItem getJMenuItemBonifieur() {
		if (jMenuItemBonifieur == null) {
			jMenuItemBonifieur = new JCheckBoxMenuItem();
			jMenuItemBonifieur.setText("Bonifier le cahier");
			jMenuItemBonifieur.setSelected(true);
			jMenuItemBonifieur.setMnemonic(java.awt.event.KeyEvent.VK_B);
			jMenuItemBonifieur.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Preference.getIntance().setBoolean(Preference.P_MODE_BONIFIEUR, jMenuItemBonifieur.isSelected());
					getCahierCourant().repaint();
				}
			});
		}
		return jMenuItemBonifieur;
	}

	private SliderMenuItem getJMenuDelaisDebogueur() {
		if (jMenuItemDebogueur == null) {
			jMenuItemDebogueur = new SliderMenuItem();
			jMenuItemDebogueur.setPaintLabels(true);
		}
		return jMenuItemDebogueur;
	}

	private JMenuItem getJMenuFormater() {
		if (jMenuItemFormater == null) {
			jMenuItemFormater = new JMenuItem();
			jMenuItemFormater.setText("Indenter le livre");
			jMenuItemFormater.setMnemonic(java.awt.event.KeyEvent.VK_I);
			jMenuItemFormater.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					Formater.action(getCahierCourant(), linotte);
				}
			});
		}
		return jMenuItemFormater;
	}

	private JMenuItem getJMenuRechercher() {
		if (jMenuItemRechercher == null) {
			jMenuItemRechercher = new JMenuItem();
			jMenuItemRechercher.setText("Rechercher...");
			jMenuItemRechercher.setMnemonic(java.awt.event.KeyEvent.VK_R);
			jMenuItemRechercher.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK));
			jMenuItemRechercher.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					findAndReplace.setVisible(true);
				}
			});
		}
		return jMenuItemRechercher;
	}

	private JMenuItem getJMenuLaToile() {
		if (jMenuItemLaToile == null) {
			jMenuItemLaToile = new JMenuItem();
			jMenuItemLaToile.setText("Afficher la toile");
			jMenuItemLaToile.setMnemonic(java.awt.event.KeyEvent.VK_T);
			jMenuItemLaToile.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					((JPanelLaToile) toile.getPanelLaToile()).getToileParent().setVisible(true);
				}
			});
		}
		return jMenuItemLaToile;
	}

	private JMenuItem getJMenuManageur(final String url) {
		if (jMenuItemManageur == null) {
			jMenuItemManageur = new JMenuItem();
			jMenuItemManageur.setText("Manageur de greffons Java...");
			jMenuItemManageur.setMnemonic(java.awt.event.KeyEvent.VK_F);
			jMenuItemManageur.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					GreffonsManager.ouvrir(linotte, Atelier.this, url);
				}
			});
		}
		return jMenuItemManageur;
	}

	private JMenuItem getJMenuGreffonLinotte() {
		if (jMenuItemGreffonLinotte == null) {
			jMenuItemGreffonLinotte = new JMenuItem();
			jMenuItemGreffonLinotte.setText("Importer un greffon Linotte...");
			jMenuItemGreffonLinotte.setMnemonic(java.awt.event.KeyEvent.VK_L);
			jMenuItemGreffonLinotte.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {

					int returnVal = fileChooser_greffonJava.showOpenDialog(FrameDialog.frame);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						try {
							if (GestionDesGreffons.importerGreffonsLinotte(new File(fileChooser_greffonJava.getSelectedFile().getAbsolutePath())))
								JOptionPane.showMessageDialog(atelier, "Greffon Linotte importé. Relancer l'atelier pour le prendre en compte.",
										"Importation d'un greffon Linotte", JOptionPane.INFORMATION_MESSAGE);
							else
								JOptionPane.showMessageDialog(atelier, "Greffon Linotte n'a pas été importé.", "Importation d'un greffon Linotte",
										JOptionPane.INFORMATION_MESSAGE);
						} catch (Exception e1) {
						}
					}
				}

			});
		}
		return jMenuItemGreffonLinotte;
	}

	private JMenuItem getJMenuManageurStyle() {
		if (jMenuItemManageurStyle == null) {
			jMenuItemManageurStyle = new JMenuItem();
			jMenuItemManageurStyle.setText("Polices et couleurs...");
			jMenuItemManageurStyle.setMnemonic(java.awt.event.KeyEvent.VK_Y);
			jMenuItemManageurStyle.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					StyleManageur.ouvrir(Atelier.this);
				}
			});
		}
		return jMenuItemManageurStyle;
	}

	/**
	 * This method initializes jMenuLinotte
	 * 
	 * @return JMenu
	 */
	private JMenu getJMenuLinotte() {
		if (jMenuLinotte == null) {
			jMenuLinotte = createJMenuNonOpaque("       Plus       ", Ressources.getImageIconePlusClaire("bandeau/quote.png"),
					Ressources.getImageIcon("bandeau/quote.png"));
			jMenuLinotte.setBorderPainted(menuBarBordure);
			jMenuLinotte.setVerticalTextPosition(buttonTextHorizontale);
			jMenuLinotte.setHorizontalTextPosition(buttonTextVerticale);
			if (!Version.isPro())
				jMenuLinotte.add(getJMenuItemMerci()).setIcon(Ressources.getImageIcon("fenetre/heart.png"));
			jMenuLinotte.add(getJMenuItemAPropos()).setIcon(Ressources.getImageIcon("help-browser.png"));
			jMenuLinotte.setMnemonic(KeyEvent.VK_I);
			jMenuLinotte.setName("Aide");
		}
		return jMenuLinotte;
	}

	/**
	 * This method initializes jMenuItemApropos
	 * 
	 * @return JMenuItem
	 */
	private JMenuItem getJMenuItemAPropos() {
		if (jMenuItemApropos == null) {
			jMenuItemApropos = new JMenuItem();
			jMenuItemApropos.setText("A propos");
			jMenuItemApropos.setMnemonic(KeyEvent.VK_A);
			jMenuItemApropos.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					apropos.setVisible(true);
				}
			});
		}
		return jMenuItemApropos;
	}

	private JMenuItem getJMenuItemMerci() {
		if (jMenuItemMerci == null) {
			jMenuItemMerci = new JMenuItem();
			jMenuItemMerci.setText("Remerciement");
			jMenuItemMerci.setMnemonic(KeyEvent.VK_R);
			jMenuItemMerci.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					merci.setVisible(true);
				}
			});
		}
		return jMenuItemMerci;
	}

	/**
	 * Créée un menu contenant tout les verbes du langage
	 * 
	 * @param racine
	 *            L'élément XML racine
	 */
	private void creerJMenuVerbe(Element racine) {

		XMLIterator l1 = new XMLIterator(racine.getElementsByTagName("parametre"));

		List<String> liste = new ArrayList<String>();
		while (l1.hasNext()) {
			Noeud node = l1.next();
			XMLIterator it = node.getFils();
			while (it.hasNext()) {
				NExpression n = (NExpression) it.next();
				if ("aide".equals(node.getAttribut("nom"))) {
					String clef = n.getValeur().toString();
					liste.add(clef);
					if (n.getAttribut("aide") != null) {
						verbes_aide.put(clef, n.getAttribut("aide"));
					}
					if (n.getAttribut("texte") == null && n.getAttribut("math") == null && n.getAttribut("boucle") == null) {
						verbes_imperatif.put(clef, filtre(n.getAttribut("imperatif")));
						verbes_present.put(clef, filtre(n.getAttribut("present")));
						verbes_icone.put(clef, n.getAttribut("icone") != null);
					} else {
						if (n.getAttribut("texte") != null) {
							verbes_conditions.put(clef, filtre(n.getAttribut("texte")));
							verbes_icone.put(clef, n.getAttribut("icone") != null);
							verbes_imperatif.put(clef, filtre(n.getAttribut("texte")));
							verbes_present.put(clef, filtre(n.getAttribut("texte")));
						} else {
							if (n.getAttribut("math") != null) {
								verbes_mathematiques.put(clef, filtre(n.getAttribut("math")));
								verbes_icone.put(clef, n.getAttribut("icone") != null);
								verbes_imperatif.put(clef, filtre(n.getAttribut("math")));
								verbes_present.put(clef, filtre(n.getAttribut("math")));
							} else {
								if (n.getAttribut("boucle") != null) {
									verbes_boucle.put(clef, filtre(n.getAttribut("boucle")));
									verbes_icone.put(clef, n.getAttribut("icone") != null);
									verbes_imperatif.put(clef, filtre(n.getAttribut("boucle")));
									verbes_present.put(clef, filtre(n.getAttribut("boucle")));
								}
							}
						}
					}
				}
			}
		}
		Collections.sort(liste);
		Iterator<String> i = liste.iterator();
		while (i.hasNext()) {
			String s = i.next();
			creerJMenuVerbe(s, s);
		}
	}

	private String filtre(String f) {
		String user = System.getProperty("user.name");
		return f.replaceAll(":auteur:", user).replaceAll(":ligne:", "\n").replaceAll(":tab:", "\t").replaceAll(":version:", linotte.getGrammaire().getSPEC())
				.replaceAll(":g:", "\"");
	}

	public void copierTableau() {
		String selection = jEditorPaneTableau.getSelectedText();
		if (selection == null)
			selection = jEditorPaneTableau.getText();
		StringSelection clipString = new StringSelection(selection);
		clipbd.setContents(clipString, clipString);
	}

	public void effacerAudit() {
		synchronized (sortieAudit) {
			try {
				sortieAudit.remove(0, sortieAudit.getLength());
			} catch (BadLocationException e2) {
				e2.printStackTrace();
			}
		}
	}

	public void windowActivated(WindowEvent arg0) {
	}

	public void windowClosed(WindowEvent arg0) {
	}

	public void windowClosing(WindowEvent arg0) {
		if (verifierLivre("Voulez-vous vraiment quitter l'Atelier Linotte ?")) {
			// Sauvegarder les onglets automatiques ?
			Preference.getIntance().nettoyer(Preference.P_FICHIER);
			if (Preference.getIntance().getBoolean(Preference.P_MODE_SAVE_WORKSPACE)) {
				List<String> fichiers = cahierOnglet.getFichiersOuverts();
				int i = 0;
				for (String fichier : fichiers) {
					i++;
					Preference.getIntance().setProperty(Preference.P_FICHIER + "_" + i, fichier);
				}
			}
			// Sauvegarde de l'historique des fichiers
			Preference.getIntance().nettoyer(Preference.P_HISTORIQUE);
			int i = 0;
			for (OuvrirLivreAction fichier : listHistorique) {
				i++;
				Preference.getIntance().setProperty(Preference.P_HISTORIQUE + "_" + i, fichier.info.getAbsolutePath());
			}

			Preference.getIntance().save();
			GestionnaireFavoris.getInstance().enregistrerFavoris();
			StyleBuilder.enregistrer();
			System.exit(0);
		}
	}

	public void windowDeactivated(WindowEvent arg0) {
	}

	public void windowDeiconified(WindowEvent arg0) {
	}

	public void windowIconified(WindowEvent arg0) {
	}

	public void windowOpened(WindowEvent arg0) {
	}

	public boolean verifierLivre(String message) {
		if (cahierOnglet.verifierEtat(EtatCachier.MODIFIE)) {
			int result = JOptionPane.showConfirmDialog(this, message, "Un livre n'est pas sauvegardé !", JOptionPane.YES_NO_OPTION);
			return result == JOptionPane.YES_OPTION;
		} else {
			return true;
		}
	}

	private void creerJMenuEspeces(Linotte linotte) {
		List<String> noms = new ArrayList<String>();
		Map<String, Prototype> map = new HashMap<String, Prototype>();
		for (Prototype e : linotte.especeModeleMap) {
			String n = e.getNom().toString();
			noms.add(n);
			map.put(n, e);
			Set<String> slots = e.retourneSlotsGreffons();
			Set<String> slotsLinotte = e.retourneSlotsLinotte();
			Set<String> slots_complet = new HashSet<String>();
			for (String slt : slotsLinotte) {
				slots_complet.add(slt + "()");
			}
			for (String slt : slots) {
				// On récupère les paramètres :
				AKMethod akmethod = e.retourneSlotGreffon(slt);
				if (akmethod instanceof JavaMethod) {
					JavaMethod method = (JavaMethod) akmethod;
					slt += method.parametres();
				}
				slots_complet.add(slt);
			}
			if (slots_complet != null && slots_complet.size() > 0)
				slots_prototype.put(n, slots_complet);
		}
		Collections.sort(noms);
		for (String string : noms) {
			Prototype e = map.get(string);
			String texte = e.getGreffon() == null ? e.toTexte() : GreffonsChargeur.getInstance().getGreffon(string).toTexte();
			String aide = e.getGreffon() == null ? null : GreffonsChargeur.getInstance().getGreffon(string).toString();
			verbes_especes.put(string, string);
			verbes_icone.put(string, false);
			verbes_imperatif.put(string, texte);
			verbes_present.put(string, texte);
			if (aide != null)
				verbes_aide.put(string, aide);
		}
		for (String n : noms) {
			creerJMenuVerbe(n + ((map.get(n).getGreffon() == null) ? "" : ""), n);
		}

	}

	private void creerJMenuCouleurs() {
		List<String> couleurs = Couleur.retourneCouleurs();
		Collections.sort(couleurs);
		int i = 0;
		int m = 1;
		JMenu jMenuCouleurs = null;
		for (String c : couleurs) {
			if (i % 40 == 0) {
				jMenuCouleurs = getJMenuCouleurs(m);
				jMenuVerbier.add(jMenuCouleurs);
				m++;
			}
			JMenuItem itemCouleur = new JMenuItem(c);
			Image ic = createImage(15, 15);
			Graphics graphics = ic.getGraphics();
			graphics.setColor(Couleur.retourneCouleur(c));
			graphics.fillRect(0, 0, 15, 15);
			itemCouleur.setIcon(new ImageIcon(ic));
			itemCouleur.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					getCahierCourant().ecrireInsertAuCurseurCachier(((JMenuItem) e.getSource()).getText());
				}
			});
			jMenuCouleurs.add(itemCouleur);
			i++;
		}
	}

	private JMenuItem creerMenu(String commande) {
		JMenuItem jMenu = new JMenu();
		jMenu.setActionCommand(commande);
		jMenu.setText(commande);
		jMenu.setBackground(menuBarColor);
		jMenu.setForeground(textBarColor);
		jMenu.setBorderPainted(menuBarBordure);
		jMenu.setIcon(Ressources.getImageIcon("system-shutdown.png"));
		return jMenu;
	}

	public LaToile getToile() {
		return toile;
	}

	public Inspecteur getInspecteur() {
		return inspecteur;
	}

	/**
	 * @param file
	 * @throws IOException
	 */
	public void ouvrirLivre(File file) throws IOException {
		if (!cahierOnglet.estIlOuverts(file)) {
			creerCahierCourant(false);
			getCahierCourant().chargerFichier(file);
			getCahierCourant().forceStyle();
			GestionnaireFavoris.getInstance().peuplerCahier(getCahierCourant());
			Preference.getIntance().setProperty(Preference.P_DIRECTORY, file.getAbsolutePath());
			ajouterHistorique(file, true);
		}
	}

	private void ajouterHistorique(File file, boolean recharger) {
		synchronized (listHistorique) {
			while (listHistorique.size() > 19)
				listHistorique.remove(0);
			supprimerHistorique(file);
			listHistorique.add(new OuvrirLivreAction(file));
			if (recharger)
				rechargerHistorique();
		}
	}

	private void supprimerHistorique(File file) {
		OuvrirLivreAction ouvrirLivreAction = null;
		for (OuvrirLivreAction element : listHistorique) {
			try {
				if (element.info.getCanonicalPath().equals(file.getCanonicalPath())) {
					ouvrirLivreAction = element;
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (ouvrirLivreAction != null) {
			listHistorique.remove(ouvrirLivreAction);
		}
	}

	public String getStructureLivre() {
		return verbes_imperatif.get("+Livre");
	}

	public void renommerLivre(File ancien, File nouveau) throws IOException {
		Cahier cahier = cahierOnglet.retourneCahier(ancien);
		if (cahier != null) {
			cahier.renommerFichier(nouveau);
		}
	}

	public Onglets getCahierOnglet() {
		return cahierOnglet;
	}

	private JMenu createJMenuNonOpaque(String titre, final ImageIcon iconeClair, final ImageIcon icone) {
		// http://java-swing-tips.blogspot.fr/2009/08/jmenubar-background-image.html
		// 

		JMenu jMenu = new JMenu(titre) {

			ArrowIcon iconRenderer = new ArrowIcon(SwingConstants.SOUTH, true);

			@Override
			protected void fireStateChanged() {
				ButtonModel m = getModel();
				if (m.isPressed() && m.isArmed()) {
					setIcon(iconeClair);
					setOpaque(true);
				} else if (m.isSelected()) {
					setIcon(iconeClair);
					setOpaque(true);
				} else if (isRolloverEnabled() && m.isRollover()) {
					setIcon(iconeClair);
				} else {
					setIcon(icone);
					setOpaque(false);
				}
				super.fireStateChanged();
			};

			@Override
			public void updateUI() {
				super.updateUI();
				setOpaque(false); // Motif lnf
			}

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				Dimension d = this.getPreferredSize();
				int x = Math.max(0, d.width - iconRenderer.getIconWidth() - 3);
				iconRenderer.paintIcon(this, g, x, 15);
			}

		};

		jMenu.setIcon(icone);
		if ("Windows XP".equals(System.getProperty("os.name"))) {
			jMenu.setBackground(new Color(0, 0, 0, 0)); // XXX Windows XP lnf?
		}

		return jMenu;
	}

	private void creationSousMenuVerbier(final String sConstructionMenus) {
		if (linotte.getLangage().isLegacy()) {
			jMenuVerbier.add(getJMenuItemPresent());
			jMenuVerbier.add(getJMenuItemImperatif());
			jMenuVerbier.addSeparator();
		}
		getJMenuCondition();
		getJMenuMathematiques();
		getJMenuBoucle();
		splashWindow1.setProgressValue(7, sConstructionMenus + " : menu des espèces");
		getJMenuEspeces();
		splashWindow1.setProgressValue(7, sConstructionMenus + " : menu des verbes");
		creerJMenuVerbe(linotte.getGrammaire().getDefinitionsXml());
		splashWindow1.setProgressValue(7, sConstructionMenus + " : menu des prototypes");

		creerJMenuEspeces(linotte);
		jMenuVerbier.addSeparator();
		jMenuVerbier.add(getJMenuEspeces());
		jMenuVerbier.add(getJMenuCondition());
		jMenuVerbier.add(getJMenuBoucle());
		jMenuVerbier.add(getJMenuMathematiques());
		splashWindow1.setProgressValue(7, sConstructionMenus + " : menu des couleurs");
		jMenuVerbier.addSeparator();
		creerJMenuCouleurs();

		if (jMenuItemPresent != null) {
			jMenuItemPresent.setSelected(affichagePresent);
			jMenuItemImperatif.setSelected(!affichagePresent);
		}
	}

	/**
	 * Ajoute un clipnotte dans la zone projet de l'Atelier
	 * 
	 * @param jComponent
	 * @param titre
	 */
	public static void ajouteExtension(Component jComponent, String titre) {
		explorateur.ajouteExtension(jComponent, titre);
	}

	/**
	 * Supprime un clipnotte dans la zone projet de l'Atelier
	 * 
	 * @param jComponent
	 */
	public static void supprimeExtension(Component jComponent) {
		explorateur.supprimeExtension(jComponent);
	}

	public static String getTitre() {
		return "Atelier de programmation " + ((linotte == null || linotte.getLangage() == null) ? "Linotte" : linotte.getLangage().getNom());
	}

}
