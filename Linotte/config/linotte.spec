Summary: Langage de programmation Linotte
Name: langagelinotte
Version: 2.07.07
Release: fedora
License: GPL
Group: Applications/Development
URL: http://langagelinotte.free.fr/
Vendor: Ronan Mounès
Packager: Ronan Mounès
Prefix: %{_prefix}/langagelinotte
BuildArchitectures: noarch
Requires: java

%global _binaries_in_noarch_packages_terminate_build 0

%description
Langage de programmation en français.
Linotte est le langage de programmation libre (licence GPL V3) dont la syntaxe est inspirée du français. 

%prep

%build

%install

%clean

%post
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :
chmod +x /usr/bin/linotte

%files
/usr/bin/linotte
/usr/share/langagelinotte/
/usr/share/mime/packages/greffonlinotte.xml
/usr/share/mime/packages/linotte.xml
/usr/share/pixmaps/application-greffon.png
/usr/share/pixmaps/application-livre.png
/usr/share/pixmaps/astronotte.png
/usr/share/pixmaps/x-atelierlinotte.png
/usr/share/applications/astronotte.desktop
/usr/share/applications/greffonlinotte.desktop
/usr/share/applications/jinotte.desktop
/usr/share/applications/linotte.desktop
/usr/share/icons/gnome/scalable/mimetypes/application-greffon.svg
/usr/share/icons/gnome/scalable/mimetypes/application-livre.svg
