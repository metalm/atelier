﻿
$ErrorActionPreference = 'Stop'; # stop on all errors

$binRoot = $(Split-Path -parent $MyInvocation.MyCommand.Definition)
$toolsDir = Join-Path $binRoot "linotte"
$exeDir = Join-Path $toolsDir "Linotte.exe"

$packageName= 'linotte' 
$url        = 'https://bitbucket.org/metalm/langagelinotte/downloads/Linotte_2.07.06.zip'

$packageArgs = @{
  packageName   = $packageName
  unzipLocation = $toolsDir
  url           = $url
  checksum      = 'FCAE0ECE2AC807894CEC79D9BEA0B06A4C56360320749F74C25602C7D372F707'
  checksumType  = 'sha256'
}

Install-ChocolateyZipPackage @packageArgs 

Install-ChocolateyShortcut -shortcutFilePath (Join-Path $env:ALLUSERSPROFILE "Microsoft\Windows\Start Menu\Atelier Linotte.lnk") -TargetPath (Join-Path $toolsDir "Linotte.exe") `
    