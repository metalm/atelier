Pour contribuer au projet Linotte :

* Avoir le logiciel Eclipse
* Importer le projet Linotte
* Corriger les classpath le cas échéant
* Le "main" principal de l'Atelier se trouve dans la classe : org.linotte.frame.Atelier
* Autres mains : 
* * Webonotte seul : org.linotte.web.serveur.Webonotte
* * Ligne de commande : console.Jinotte ou Jinotte
* * Installateur : console.Installateur

Pour construire les livrables :

* Avoir un JRE récent installé sur sa machine
* Récupérer la dernière version de http://launch4j.sourceforge.net/
* Puis, avec Ant, lancer les targets du fichier build.xml : dist, archive, package, billgates

Voici un schéma décrivant l'architecture de la Machine Virtuelle Linotte :

![archi277.png](https://bytebucket.org/metalm/atelier/raw/49235157e48d8487dbdbd601ab69ab4f48ee9839/Linotte/doc/archi277.png)